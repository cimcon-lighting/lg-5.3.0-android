
package com.cl.lg.pojo.saml_details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("kClientID")
    @Expose
    private String kClientID;
    @SerializedName("isSamlSupport")
    @Expose
    private String isSamlSupport;

    @SerializedName("tenantid")
    @Expose
    private String tenantid;
    @SerializedName("audiance_type")
    @Expose
    private String  audiance_type;

    public String getkClientID() {
        return kClientID;
    }

    public void setkClientID(String kClientID) {
        this.kClientID = kClientID;
    }

    public String getTenant_id() {
        return tenantid;
    }

    public void setTenant_id(String tenant_id) {
        this.tenantid = tenant_id;
    }

    public String getAudiance_type() {
        return audiance_type;
    }

    public void setAudiance_type(String audiance_type) {
        this.audiance_type = audiance_type;
    }

    public String getIsSamlSupport() {
        return isSamlSupport;
    }

    public void setIsSamlSupport(String isSamlSupport) {
        this.isSamlSupport = isSamlSupport;
    }

}
