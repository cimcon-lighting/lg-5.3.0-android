
package com.cl.lg.pojo.SentCommand;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendcommandBody {

    @SerializedName("SendCommandBodyInner")
    @Expose
    private List<SendCommandBodyInner> sendCommandBodyInner = null;

    public List<SendCommandBodyInner> getSendCommandBodyInner() {
        return sendCommandBodyInner;
    }

    public void setSendCommandBodyInner(List<SendCommandBodyInner> sendCommandBodyInner) {
        this.sendCommandBodyInner = sendCommandBodyInner;
    }

}
