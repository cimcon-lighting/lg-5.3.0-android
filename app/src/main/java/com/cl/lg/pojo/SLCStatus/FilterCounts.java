package com.cl.lg.pojo.SLCStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterCounts {

    @SerializedName("commFaults")
    @Expose
    private Integer commFaults;
    @SerializedName("driverFault")
    @Expose
    private Integer driverFault;
    @SerializedName("photoCellFault")
    @Expose
    private Integer photoCellFault;
    @SerializedName("controllerOn")
    @Expose
    private Integer controllerOn;
    @SerializedName("controllerOff")
    @Expose
    private Integer controllerOff;
    @SerializedName("controllerDim")
    @Expose
    private Integer controllerDim;

    public Integer getCommFaults() {
        return commFaults;
    }

    public void setCommFaults(Integer commFaults) {
        this.commFaults = commFaults;
    }

    public Integer getDriverFault() {
        return driverFault;
    }

    public void setDriverFault(Integer driverFault) {
        this.driverFault = driverFault;
    }

    public Integer getPhotoCellFault() {
        return photoCellFault;
    }

    public void setPhotoCellFault(Integer photoCellFault) {
        this.photoCellFault = photoCellFault;
    }

    public Integer getControllerOn() {
        return controllerOn;
    }

    public void setControllerOn(Integer controllerOn) {
        this.controllerOn = controllerOn;
    }

    public Integer getControllerOff() {
        return controllerOff;
    }

    public void setControllerOff(Integer controllerOff) {
        this.controllerOff = controllerOff;
    }

    public Integer getControllerDim() {
        return controllerDim;
    }

    public void setControllerDim(Integer controllerDim) {
        this.controllerDim = controllerDim;
    }

}