package com.cl.lg.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.DashboardCounts.LampType;
import com.cl.lg.pojo.GatewayDetails.GatewayMaster;
import com.cl.lg.pojo.LangType;
import com.cl.lg.pojo.Map.Datum;
import com.cl.lg.pojo.Map.MapMaster;
import com.cl.lg.pojo.Parameters.ParametersMasters;
import com.cl.lg.pojo.StatusResponse2;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.cl.lg.utils.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.maps.model.UrlTileProvider;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.maps.android.MarkerManager;
import com.google.maps.android.SphericalUtil;

import net.sharewire.googlemapsclustering.Cluster;
import net.sharewire.googlemapsclustering.ClusterItem;
import net.sharewire.googlemapsclustering.ClusterManager;
import net.sharewire.googlemapsclustering.DefaultIconGenerator;
import net.sharewire.googlemapsclustering.IconStyle;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapFragment extends Fragment implements OnMapReadyCallback,
        View.OnClickListener, ClusterManager.Callbacks {

    @BindView(R.id.map)
    MapView mMapView;

    @BindView(R.id.ivCommFault)
    ImageView ivCommFault;

    @BindView(R.id.ivGatwayDisconnected)
    ImageView ivGatwayDisconnected;

    @BindView(R.id.ivLampoff)
    ImageView ivLampoff;

    @BindView(R.id.ivLampOn)
    ImageView ivLampOn;

    @BindView(R.id.ivLampDim)
    ImageView ivLampDim;

    @BindView(R.id.ivGatwayConnected)
    ImageView ivGatwayConnected;

    @BindView(R.id.ivNeverCommunicated)
    ImageView ivNeverCommunicated;

    @BindView(R.id.ivPowerLoss)
    ImageView ivPowerLoss;

    @BindView(R.id.ivEarth)
    ImageView ivEarth;

    @BindView(R.id.btnLamptype)
    ImageView btnLamptype;

    @BindView(R.id.ivUserInfo)
    ImageView ivUserInfo;

    private GoogleMap googleMapGlobal;
    OnMapReadyCallback objOnMapReadyCallback;

    testCluster mClusterTest;
    ProgressDialog dialog;
    ImageView marker_image;
    ImageView marker_image_p;

    Utils objUtils;
    String body;

    View mCustomMarkerView;
    View mCustomMarkerViewP;

    View view;
    Bundle savedInstanceState;
    API objApi;
    String tokenLogin;
    SharedPreferences spf;
    java.util.List<Datum> mList;

    boolean isSelectedCF = false;
    boolean isSelectedDim = false;
    boolean isLampoff = false;
    boolean isampOn = false;
    boolean isGatwayDisconnected = false;
    boolean isGatwayConnected = false;
    boolean isPowerLoss = false;
    boolean isNeverCommunicated = false;

    String SelectedCF = "";
    String SelectedDim = "";
    String SelectedLampoff = "";
    String SelectedLampOn = "";
    String SelectedGatwayDisconnected = "";
    String SelectedGatwayConnected = "";
    String SelectedPowerLoss = "";
    String NeverCommunicated = "";
    String minX = "-20.20264";
    String minY = "0";
    String maxX = "90.11954";
    String maxY = "90.11954";

    private String[] PERMISSIONS = {
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_NETWORK_STATE
    };
    private static final int PERMISSION_ALL = 1;
    private GPSTracker gps;

    private float previousZoomLevel = -1.0f;
    private boolean isZooming = false;

    ProgressDialog dialogForLatlong;
    int selectedMapIndex;
    String c_type;

    boolean flagForAllPermission = false;

    int selectedLangIndex;
    AlertDialog dialogSelection;
    ArrayList<LangType> mListMapTypes;

    private FirebaseAnalytics mFirebaseAnalytics;
    String latStr, lngStr;
    Double lat, lng;
    String lampTypeId;
    ArrayList<LampType> mLampType;

    String type;
    String typeMessage;
    boolean isZigbeeContains;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map, null);
        this.savedInstanceState = savedInstanceState;
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);

        mCustomMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        mCustomMarkerViewP = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_p, null);
        marker_image = mCustomMarkerView.findViewById(R.id.marker_image);
        marker_image_p = mCustomMarkerViewP.findViewById(R.id.marker_image_p);
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        type = spf.getString(AppConstants.APP_TYPE, "");


        c_type = spf.getString(AppConstants.CLIENT_TYPE, "");
        isZigbeeContains=spf.getBoolean(AppConstants.isZigbeeContains,false);

        lampTypeId = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
        tokenLogin = spf.getString(AppConstants.ACCESS_TOKEN, "");

        dialogForLatlong = new ProgressDialog(getActivity());
        dialogForLatlong.setMessage(getResources().getString(R.string.please_wait));
        dialogForLatlong.setCancelable(false);

        mLampType = Utils.getLampType(spf);
        objUtils = new Utils();
        objApi = new LG().networkCall(getActivity(), false);

        selectedMapIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 0); // satellite so default 1
        mListMapTypes = new ArrayList<>();

        typeMessage = objUtils.getStringResourceByName(getActivity(), spf.getString(AppConstants.APP_TYPE_MSG, ""));

        ivGatwayConnected.setOnClickListener(this);
        ivLampDim.setOnClickListener(this);
        ivLampOn.setOnClickListener(this);
        ivLampoff.setOnClickListener(this);
        ivGatwayDisconnected.setOnClickListener(this);
        ivCommFault.setOnClickListener(this);
        ivNeverCommunicated.setOnClickListener(this);
        ivPowerLoss.setOnClickListener(this);
        ivUserInfo.setOnClickListener(this);

        latStr = spf.getString(AppConstants.LATTITUDE, "").toString();
        lngStr = spf.getString(AppConstants.LONGITUDE, "").toString();

        if (latStr.equals("") || lngStr.equals("")) {
            lat = 0.0;
            lng = 0.0;
        } else {
            lat = Double.valueOf(spf.getString(AppConstants.LATTITUDE, ""));
            lng = Double.valueOf(spf.getString(AppConstants.LONGITUDE, ""));
        }

        if (isZigbeeContains) {
            ivGatwayConnected.setVisibility(View.VISIBLE);
            ivGatwayDisconnected.setVisibility(View.VISIBLE);
            ivPowerLoss.setVisibility(View.VISIBLE);
        } else {
            ivGatwayConnected.setVisibility(View.GONE);
            ivGatwayDisconnected.setVisibility(View.GONE);
            ivPowerLoss.setVisibility(View.GONE);
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "map", null);

        mList = new ArrayList<>();

        if (Utils.isInternetAvailable(getActivity())) {
            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            mMapView.onCreate(savedInstanceState);
            mMapView.onResume(); // needed to get the map to display immediately

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                request6plus();
            } else {
                getLocation();
            }
            //getAllSLCData(client_id); - get all data
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }

        ivEarth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                    ShowDialog(mListMapTypes, getResources().getString(R.string.select_map_type));
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
            }
        });

        setMapType();
        //((MainActivity)getActivity()).selectMap(true);

        ((MainActivity) getActivity()).selectDashboard(false);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(true);
        ((MainActivity) getActivity()).selectMenu(false);

        btnLamptype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {

                    String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "selectLampType";
                    Log.d(AppConstants.TAG, "Event:" + event_name);
                    Bundle bundleAnalytics = new Bundle();
                    bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);


                    objUtils.ShowLampTypeDialog(mLampType, getResources().getString(R.string.strLampTypTitle), getActivity(), spf, new Utils.ClickLampType() {
                        @Override
                        public void setOnClickLampType(com.cl.lg.pojo.DashboardCounts.LampType objOnClickLampType) {

                            lampTypeId = objOnClickLampType.getLampTypeID();
                            spf.edit().putString(AppConstants.SELECTED_LAMP_TYPE_ID, objOnClickLampType.getLampTypeID()).apply();
                            spf.edit().putString(AppConstants.SELECTED_LAMP_TYPE, objOnClickLampType.getLampType()).apply();


                            getParamters(lampTypeId);

                        }
                    });
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
            }
        });
    }

    void setMapType() {

        mListMapTypes.clear();

        LangType obj1 = new LangType();
        obj1.setKeyLable(getString(R.string.key_google_map_standard));
        obj1.setValue(getString(R.string.google_map_standard));
        obj1.setPosition(0);
        obj1.setStyle(false);

        if (selectedMapIndex == 0)
            obj1.setChecked(true);
        else
            obj1.setChecked(false);
        mListMapTypes.add(obj1);

        LangType obj2 = new LangType();
        obj2.setKeyLable(getString(R.string.key_google_map_satellite));
        obj2.setValue(getString(R.string.google_map_satellite));
        obj2.setPosition(1);
        obj2.setStyle(false);
        if (selectedMapIndex == 1)
            obj2.setChecked(true);
        else
            obj2.setChecked(false);
        mListMapTypes.add(obj2);

        LangType obj3 = new LangType();
        obj3.setKeyLable(getString(R.string.key_google_map_hybride));
        obj3.setValue(getString(R.string.google_map_hybride));
        obj3.setPosition(2);
        obj3.setStyle(false);
        if (selectedMapIndex == 2)
            obj3.setChecked(true);
        else
            obj3.setChecked(false);
        mListMapTypes.add(obj3);

        LangType obj4 = new LangType();
        obj4.setKeyLable(getString(R.string.key_open_street_standard));
        obj4.setValue(getString(R.string.open_street_standard));
        obj4.setPosition(3);
        obj4.setStyle(true);
        obj4.setMapLink(AppConstants.OPEN_STREET_MAP_STANDARD_URL);
        if (selectedMapIndex == 4)
            obj4.setChecked(true);
        else
            obj4.setChecked(false);
        mListMapTypes.add(obj4);

    }

    void ShowDialog(final List<LangType> objList, String title) {

        String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "selectLampType";
        Log.d(AppConstants.TAG, "Event:" + event_name);
        Bundle bundleAnalytics = new Bundle();
        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_title, null);

        TextView tvTitle1 = view.findViewById(R.id.tvDialogTitle1);
        tvTitle1.setText(title);
        builder.setCustomTitle(view);
        //builder.setTitle(title);

        int selectedIndex;

        //getting fresh shared preference values ...!
        selectedLangIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 0);

        selectedIndex = selectedLangIndex;

        final String[] ary = new String[objList.size()];

        for (int i = 0; i < objList.size(); i++) {
            ary[i] = objList.get(i).getValue();
        }

        builder.setSingleChoiceItems(ary, selectedIndex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog1, int which) {
                objList.get(which).setChecked(true);

                LangType objMapTypePojo = objList.get(which);

                spf.edit().putString(AppConstants.SELCTED_MAP_TYPE, objMapTypePojo.getValue().toString()).apply();
                spf.edit().putString(AppConstants.SELECTED_MAP_TYPE_KEY, objMapTypePojo.getKeyLable().toString()).apply();
                spf.edit().putInt(AppConstants.SELECTED_MAP_INDEX, which).apply();
                selectedMapIndex = which;

                // new Utils().setMapType(getActivity(), googleMapGlobal, spf);

                setMapType(googleMapGlobal, spf);

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //dialog.dismiss();
                        dialogSelection.cancel();
                        //if (googleMapGlobal != null) //prevent crashing if the map doesn't exist yet (eg. on starting activity)
                        //    googleMapGlobal.clear();
                    }
                }, 500);
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // create and show the alert dialog
        dialogSelection = builder.create();
        dialogSelection.show();
    }

    void getData(String minX, String minY, String maxX, String maxY, boolean isFromLampType) {
        try {
            if (!isFromLampType)
                dialogForLatlong.show();

            String bodyRaw = "{\n" +
                    "\"CommunicationFaults\":\"" + SelectedCF + "\",\n" +
                    "\"GatewayDisconnected\":\"" + SelectedGatwayDisconnected + "\",\n" +
                    "\"LampsOff\":\"" + SelectedLampoff + "\",\n" +
                    "\"LampsOn\": \"" + SelectedLampOn + "\",\n" +
                    "\"LampsDimmed\":\"" + SelectedDim + "\",\n" +
                    "\"GatewaysConnected\":\"" + SelectedGatwayConnected + "\",\n" +
                    "\"minX\": \"" + minX + "\",\n" +
                    "\"minY\":\"" + minY + "\",\n" +
                    "\"maxX\":\"" + maxX + "\",\n" +
                    "\"maxY\":\"" + maxY + "\",\n" +
                    "\"GatewaysPowerloss\":\"" + SelectedPowerLoss + "\",\n" +
                    "\"NeverCommunicated\":\"" + NeverCommunicated + "\"\n" +
                    "}";

            JSONObject jsonObj = new JSONObject(bodyRaw);
            String jsonfinal = jsonObj.toString();
            Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

            objApi.getMap(tokenLogin, lampTypeId, body, AppConstants.IS_SECURED).enqueue(new Callback<MapMaster>() {
                @Override
                public void onResponse(Call<MapMaster> call, Response<MapMaster> response) {
                    try {
                        java.util.List<Datum> clusterItems = new ArrayList<>();
                        objUtils.dismissProgressDialog(dialogForLatlong);
                        if (getActivity() != null) {
                            if (response.code() == 200) {
                                MapMaster objMapMaster = response.body();
                                if (response.body().getStatus().equalsIgnoreCase("1")) {

                                    java.util.List<Datum> objList = objMapMaster.getData();
                                    int size = objList.size();

                                    Log.i(AppConstants.TAG, "Size:" + size);

                                    clusterItems.clear();
                                    for (int i = 0; i < size; i++) {
                                        //Log.i(AppConstants.TAG, "********1");

                                        Double tempLat, tempLng;

                                        if (objList.get(i).getC()==null ||objList.get(i).getC().equals(""))
                                            tempLat = 0.0;
                                        else
                                            tempLat = objList.get(i).getC();

                                        if (objList.get(i).getC()==null || objList.get(i).getD().equals(""))
                                            tempLng = 0.0;
                                        else
                                            tempLng = objList.get(i).getD();

                                        if(tempLat!=0.0 && tempLng != 0.0) {

                                            Datum objDatum = new Datum(tempLat, tempLng);

                                            if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_OFF) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_DIMMED) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_ON) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_GATEWAYS_CONNECTED) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_GATEWAY_DISCONNECTED) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_COMM_FAULT) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_NOT_CONNECTED) ||
                                                    objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_POWER_LOSS)
                                            ) {

                                                objDatum.setA(objList.get(i).getA());
                                                objDatum.setB(objList.get(i).getB());
                                                objDatum.setC(tempLat);
                                                objDatum.setD(tempLng);
                                                objDatum.setE(objList.get(i).getE());
                                                objDatum.setF(objList.get(i).getF());
                                                objDatum.setG(objList.get(i).getG());

                                                if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_OFF))
                                                    objDatum.setTitle(getResources().getString(R.string.lamp_off));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_DIMMED))
                                                    objDatum.setTitle(getResources().getString(R.string.lamp_dim));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_ON))
                                                    objDatum.setTitle(getResources().getString(R.string.lamp_on));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_GATEWAYS_CONNECTED))
                                                    objDatum.setTitle(getResources().getString(R.string.gatway_connected));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_GATEWAY_DISCONNECTED))
                                                    objDatum.setTitle(getResources().getString(R.string.gatway_discconnected));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_COMM_FAULT))
                                                    objDatum.setTitle(getResources().getString(R.string.comm_fault));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_NOT_CONNECTED))
                                                    objDatum.setTitle(getResources().getString(R.string.planning));
                                                else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_POWER_LOSS))
                                                    objDatum.setTitle(getResources().getString(R.string.power_loss_camel));

                                                if (objList.get(i).getA().toString().equalsIgnoreCase("1")) {
                                                    objDatum.setSnippet(objList.get(i).getF());
                                                } else {
                                                    objDatum.setSnippet("SLC# " + objList.get(i).getB());//getE();
                                                }
                                                objDatum.setTag("cluster");
                                                clusterItems.add(objDatum);
                                            }
                                        }else
                                            Log.i(AppConstants.TAG,"Null Lat Lng of SLC: "+objList.get(i).getB());
                                    }

                                    CustomInfoWindowGoogleMap customInfoWindow = new CustomInfoWindowGoogleMap(getActivity());
                                    googleMapGlobal.setInfoWindowAdapter(customInfoWindow);

                                    mClusterTest.setItems(clusterItems);
                                    //Log.i(AppConstants.TAG, "**@");
                                } else {
                                    Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(), objMapMaster.getMessage()));
                                    mClusterTest.setItems(clusterItems);
                                    //googleMapGlobal.clear();
                                }
                            } else
                                objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.i("***",e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<MapMaster> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialogForLatlong);
                    //googleMapGlobal.clear();
                    Log.i("***","onFailure"+t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onClusterClick(@NonNull Cluster cluster) {
        Log.i(AppConstants.TAG, "ClusterClick");
        return true;
    }

    @Override
    public boolean onClusterItemClick(@NonNull ClusterItem clusterItem) {
        Log.i(AppConstants.TAG, "ClusterItemClick");
        return false;
    }

    public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {
        private Activity context;
        LayoutInflater inflater;
        View view;

        CustomInfoWindowGoogleMap(Activity ctx) {
            this.context = ctx;
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            //this will take our own snipet
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            //this will take default background of google map
            view = inflater.inflate(R.layout.raw_google_marker, null);
            final TextView details_tv = view.findViewById(R.id.tvInfo);
            final TextView tvtitle2 = view.findViewById(R.id.tvtitle2);
            final ImageView ivInfo = view.findViewById(R.id.ivInfo);

            try {
                net.sharewire.googlemapsclustering.Cluster objCluster = (net.sharewire.googlemapsclustering.Cluster) marker.getTag();
                if (objCluster.getItems().size() != 0) {
                    Datum clusterItem = (Datum) objCluster.getItems().get(0);
                    details_tv.setText(clusterItem.getTitle());
                    tvtitle2.setText(clusterItem.getSnippet());

                    if (clusterItem.getG().equalsIgnoreCase("DNR"))
                        ivInfo.setVisibility(View.GONE);
                    else
                        ivInfo.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {
                Log.i(AppConstants.TAG, e.getMessage());
            }
            return view;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.i("***","onResume");
            if (Utils.isInternetAvailable(getActivity())) {
                if (mMapView != null) {
                    mMapView.onResume();
                }
                /*if (googleMapGlobal != null)
                    googleMapGlobal.clear();*/
            }
        } catch (Exception e) {
            Log.e("***",e.getMessage());

        }
        ((MainActivity) getActivity()).selectMap(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            Log.i("***","onPause");
            if (Utils.isInternetAvailable(getActivity())) {
                if (mMapView != null)
                    mMapView.onPause();
            }
        } catch (Exception e) {
            Log.e("***",e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mMapView != null)
                mMapView.onDestroy();

        } catch (Exception e) {
        }

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            if (mMapView != null)
                mMapView.onLowMemory();
        } catch (Exception e) {
        }
    }

    private void request6plus() {
        if (!Utils.hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
            return;
        } else {
            getLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    flagForAllPermission = false;
                    Toast.makeText(getActivity(), getResources().getString(R.string.all_permission_granted), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onMapReady(final GoogleMap googleMap1) {
        googleMapGlobal = googleMap1;

        googleMap1.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap1.getUiSettings().setRotateGesturesEnabled(true);
        googleMap1.getUiSettings().setMapToolbarEnabled(false);
        googleMap1.getUiSettings().setZoomControlsEnabled(false);
        googleMap1.getUiSettings().setCompassEnabled(true);

        googleMap1.setOnCameraChangeListener(getCameraChangeListener());

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        googleMap1.setMyLocationEnabled(true);

        if (mClusterTest == null)
            mClusterTest = new testCluster(getActivity(), googleMap1);


        //googleMap1.setOnMarkerClickListener();
        googleMap1.setOnCameraIdleListener(mClusterTest);
        //googleMap1.setOnMarkerClickListener(mClusterTest);
        //googleMap1.setInfoWindowAdapter(mClusterTest.getMarkerManager());

        googleMap1.setOnInfoWindowClickListener(mClusterTest);
        //mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mClusterTest.setIconGenerator(new pinDefault(getActivity(), googleMap1));

        //new Utils().setMapType(getActivity(), googleMap1, spf);

        setMapType(googleMap1, spf);

        String latStr = spf.getString(AppConstants.MINX, "0.0");
        String lngStr = spf.getString(AppConstants.MINY, "0.0");

        double minx;
        double miny;

        if (latStr.equals("") && lngStr.equals("")) {
            minx = Double.valueOf(spf.getString(AppConstants.LATTITUDE, "0.0"));
            miny = Double.valueOf(spf.getString(AppConstants.LONGITUDE, "0.0"));
        } else {
            minx = Double.valueOf(latStr);
            miny = Double.valueOf(lngStr);
        }

        /*Double lat = 0.0, lng = 0.0;
        if (minx == 0.0 && miny == 0.0) {
            try {
                lat = Double.valueOf(spf.getString(AppConstants.LATTITUDE, "0.0"));
                lng = Double.valueOf(spf.getString(AppConstants.LONGITUDE, "0.0"));
            } catch (Exception e) {
            }
        } else {
            lat = minx;
            lng = miny;
        }
        */

        LatLng location = new LatLng(minx, miny);
        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(17).build();
        googleMap1.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        //setData(mClusterTest, googleMap1, 1, lat, lng);

        mClusterTest.setCallbacks(this);

        //ivCommFault.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_rect_gray));
        //isSelectedCF = true;
        //SelectedCF = "cfail";

        googleMap1.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                //do stuff here

                VisibleRegion visibleRegion = googleMap1.getProjection()
                        .getVisibleRegion();

                LatLngBounds latLngBounds = visibleRegion.latLngBounds;
                maxX = String.valueOf(latLngBounds.northeast.longitude);
                maxY = String.valueOf(latLngBounds.northeast.latitude);

                minX = String.valueOf(latLngBounds.southwest.longitude);
                minY = String.valueOf(latLngBounds.southwest.latitude);

                getData(minX, minY, maxX, maxY, false);
            }
        });
    }

    public GoogleMap.OnCameraChangeListener getCameraChangeListener() {
        return new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition position) {

                try {

                    Log.d("Zoom", "Zoom: " + position.zoom);
                    Log.d("PreviousZoom", "Zoom: " + previousZoomLevel);

                    if (previousZoomLevel != position.zoom && position.zoom < previousZoomLevel) {
                        isZooming = true;
                    } else
                        isZooming = false;

                    previousZoomLevel = position.zoom;

                    //LatLng latLng = position.target;
                    //Log.i(AppConstants.TAG, "LatLong: " + latLng.latitude + " | " + latLng.longitude);

                    VisibleRegion visibleRegion = googleMapGlobal.getProjection().getVisibleRegion();
                    double distance = SphericalUtil.computeDistanceBetween(
                            visibleRegion.farLeft, googleMapGlobal.getCameraPosition().target);

                    Log.i(AppConstants.TAG, "meters:  " + distance);

                    distance = distance / 100;
                    Log.i(AppConstants.TAG, "KM:  " + distance);

                    if (isZooming) {

                        LatLngBounds latLngBounds = visibleRegion.latLngBounds;
                        maxX = String.valueOf(latLngBounds.northeast.longitude);
                        maxY = String.valueOf(latLngBounds.northeast.latitude);

                        minX = String.valueOf(latLngBounds.southwest.longitude);
                        minY = String.valueOf(latLngBounds.southwest.latitude);

                        Log.i(AppConstants.TAG, "minX(lat):" + minX + " minY(lng): " + minY + " maxX(lat): " + maxX + " maxY(lng):" + maxY);

                        //googleMapGlobal.clear();
                        getData(minX, minY, maxX, maxY, false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivUserInfo:
                String event_name1 = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "infoDialogMap";
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME,event_name1);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                objUtils.dialog_info(getActivity());
                break;
            case R.id.ivCommFault:
                String event_name = "";
                if (flagForAllPermission) {
                    if (!isSelectedCF) {
                        ivCommFault.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isSelectedCF = true;
                        SelectedCF = AppConstants.MAP_COMM_FAULT;

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapCommunicationFault";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);

                    } else {
                        //ivCommFault.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_white));
                        ivCommFault.setBackground(null);
                        isSelectedCF = false;
                        SelectedCF = "";
                    }

                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.ivLampDim:
                if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {

                    if (flagForAllPermission) {
                        if (!isSelectedDim) {
                            ivLampDim.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));

                            isSelectedDim = true;
                            SelectedDim = AppConstants.MAP_LAMPS_DIMMED;

                            event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapDim";
                            Bundle bundleAnalytics = new Bundle();
                            bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                            Log.d(AppConstants.TAG, "Event:" + event_name);
                        } else {
                           ivLampDim.setBackground(null);
                            isSelectedDim = false;
                            SelectedDim = "";
                        }

                        if (Utils.isInternetAvailable(getActivity()))
                            getData(minX, minY, maxX, maxY, false);
                        else
                            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                    }
                } else
                    objUtils.dialogForApptype(getActivity(), typeMessage);
                break;
            case R.id.ivLampoff:

                if (flagForAllPermission) {
                    if (!isLampoff) {
                        ivLampoff.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isLampoff = true;
                        SelectedLampoff = AppConstants.MAP_LAMPS_OFF;

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapLampOff";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);
                    } else {
                        ivLampoff.setBackground(null);
                        isLampoff = false;
                        SelectedLampoff = "";
                    }

                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.ivLampOn:

                if (flagForAllPermission) {
                    if (!isampOn) {
                        ivLampOn.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isampOn = true;
                        SelectedLampOn = AppConstants.MAP_LAMPS_ON;

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapLampOn";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);
                    } else {
                        ivLampOn.setBackground(null);
                        isampOn = false;
                        SelectedLampOn = "";
                    }

                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.ivGatwayDisconnected:

                if (flagForAllPermission) {
                    if (!isGatwayDisconnected) {
                        ivGatwayDisconnected.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isGatwayDisconnected = true;
                        SelectedGatwayDisconnected = AppConstants.MAP_GATEWAY_DISCONNECTED;

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapGatewayDisconnected";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);
                    } else {
                        //ivGatwayDisconnected.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_rectangle_white));
                        ivGatwayDisconnected.setBackground(null);
                        isGatwayDisconnected = false;
                        SelectedGatwayDisconnected = "";
                    }

                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }


                break;
            case R.id.ivGatwayConnected:

                if (flagForAllPermission) {
                    if (!isGatwayConnected) {
                        ivGatwayConnected.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isGatwayConnected = true;
                        SelectedGatwayConnected = AppConstants.MAP_GATEWAYS_CONNECTED;
                        //getData(minX, minY, maxX, maxY);

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapGatewayConnected";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);

                    } else {
                        //ivGatwayConnected.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_white));
                        ivGatwayConnected.setBackground(null);
                        isGatwayConnected = false;
                        SelectedGatwayConnected = "";
                    }
                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;

            case R.id.ivPowerLoss:

                if (flagForAllPermission) {
                    if (!isPowerLoss) {
                        ivPowerLoss.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isPowerLoss = true;
                        SelectedPowerLoss = AppConstants.MAP_POWER_LOSS;

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapGatewayPowreLoss";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);
                    } else {
                        //ivPowerLoss.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_white));
                        ivPowerLoss.setBackground(null);
                        isPowerLoss = false;
                        SelectedPowerLoss = "";
                    }

                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }


                break;

            case R.id.ivNeverCommunicated:

                if (flagForAllPermission) {
                    if (!isNeverCommunicated) {
                        ivNeverCommunicated.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_skyblue));
                        isNeverCommunicated = true;
                        NeverCommunicated = AppConstants.MAP_NOT_CONNECTED;
                        //getData(minX, minY, maxX, maxY);

                        event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "mapInPlanning";
                        Bundle bundleAnalytics = new Bundle();
                        bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
                        Log.d(AppConstants.TAG, "Event:" + event_name);
                    } else {
                        //ivNeverCommunicated.setBackground(getActivity().getResources().getDrawable(R.drawable.custom_round_white));
                        ivNeverCommunicated.setBackground(null);
                        isNeverCommunicated = false;
                        NeverCommunicated = "";
                    }
                    if (Utils.isInternetAvailable(getActivity()))
                        getData(minX, minY, maxX, maxY, false);
                    else
                        Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;
        }
    }

    public class testCluster extends net.sharewire.googlemapsclustering.ClusterManager<Datum>
            implements GoogleMap.OnInfoWindowClickListener {
        /**
         * Creates a new cluster manager using the default icon generator.
         * To customize marker icons, set a custom icon generator using
         *
         * @param context
         * @param googleMap the map instance where markers will be rendered
         */
        MarkerManager mMarkerManager;
        final MarkerManager.Collection mMarkers;

        public testCluster(@NonNull Context context, @NonNull GoogleMap googleMap) {
            super(context, googleMap);
            mMarkerManager = new MarkerManager(googleMap);
            this.mMarkers = mMarkerManager.newCollection();
        }

        public MarkerManager.Collection getMarkerCollection() {
            return this.mMarkers;
        }

        public MarkerManager getMarkerManager() {
            return this.mMarkerManager;
        }

        @Override
        public void onInfoWindowClick(Marker marker) {

            try {
                net.sharewire.googlemapsclustering.Cluster objCluster = (net.sharewire.googlemapsclustering.Cluster) marker.getTag();
                if (objCluster.getItems().size() != 0) {
                    Datum clusterItem = (Datum) objCluster.getItems().get(0);

                    int deviceType = Integer.parseInt(clusterItem.getA());

                    if (type.toString().equalsIgnoreCase(AppConstants.APP_TYPE_PAID)) {
                        if (deviceType == 1) {
                            //gatway
                            String type = "";
                            if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_GATEWAYS_CONNECTED))
                                type = AppConstants.G_CONNECTED;
                            else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_GATEWAY_DISCONNECTED))
                                type = AppConstants.G_DISCONNECTED;
                            else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_POWER_LOSS))
                                type = AppConstants.G_POWERLOSS;

                            getGatwayList(type, String.valueOf(clusterItem.getB()));
                            Log.i(AppConstants.TAG, "gatway:" + type + " GatwayId:" + clusterItem.getB());

                        } else if (deviceType == 2) {
                            //SLC
                            if (!clusterItem.getG().equalsIgnoreCase("DNR")) {
                                getDataStatus(Long.parseLong(clusterItem.getB()));
                                Log.i(AppConstants.TAG, "deviceId:" + clusterItem.getB());
                            }
                        }
                    } else
                        objUtils.dialogForApptype(getActivity(), typeMessage);

                }
            } catch (Exception e) {
                Log.i(AppConstants.TAG, e.getMessage());
            }

        }
    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_google_pin, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

    public class pinDefault extends DefaultIconGenerator<Datum> {
        GoogleMap googleMap;

        /**
         * Creates an icon generator with the default icon style.
         *
         * @param context
         */
        public pinDefault(Context context, GoogleMap map) {
            super(context);
            this.googleMap = map;
        }

        @Override
        public BitmapDescriptor getClusterIcon(net.sharewire.googlemapsclustering.Cluster<Datum> cluster) {

            int size = cluster.getItems().size();
            //int size=getClusterIconBucket(cluster);

            View customMarkerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_custer, null);
            TextView objTextView = customMarkerView.findViewById(R.id.txtClusterSize);

            if (size != 0) {
                if (size > 999)
                    objTextView.setText("999+");
                else
                    objTextView.setText(String.valueOf(size));
            }

            customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
            customMarkerView.buildDrawingCache();
            Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(returnedBitmap);
            canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
            Drawable drawable = customMarkerView.getBackground();
            if (drawable != null) drawable.draw(canvas);
            customMarkerView.draw(canvas);

            //return BitmapDescriptorFactory.fromBitmap(returnedBitmap);
            return BitmapDescriptorFactory.fromBitmap(returnedBitmap);


        }

        @Override
        public void setIconStyle(@NonNull IconStyle iconStyle) {
            super.setIconStyle(iconStyle);
        }

        @NonNull
        @Override
        public BitmapDescriptor getClusterItemIcon(Datum clusterItem) {
            super.getClusterItemIcon(clusterItem);

            if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_COMM_FAULT)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.no_comm_graph_14_12, marker_image));
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_OFF)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.lamp_off_marker_29_12_, marker_image));//  R.raw.pin_gif
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_ON)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.lamp_on_4_12, marker_image));
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_DIMMED)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.lamp_dim_4_12, marker_image));
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_GATEWAYS_CONNECTED)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.g_active_on_4_12, marker_image));
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_GATEWAY_DISCONNECTED)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.gateway_off, marker_image));
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_NOT_CONNECTED)) {
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerViewP, R.drawable.never_comm_graph_14_12, marker_image_p));
            } else if (clusterItem.getG().equalsIgnoreCase(AppConstants.MAP_POWER_LOSS))
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerViewP, R.drawable.gateway_powerloss, marker_image_p));
            else
                return BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(getActivity(), mCustomMarkerView, R.drawable.custom_pin_1, marker_image));

            //return super.getClusterItemIcon(clusterItem);
        }
    }

    private void getLocation() {
        gps = new GPSTracker(getActivity());
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Log.i(AppConstants.TAG, latitude + " | " + longitude);

            if (latitude == 0.0 || longitude == 0.0) {
                dialogForLatlong.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLocation();//recursion
                    }
                }, 5000);

            } else {
                if (dialogForLatlong.isShowing())
                    dialogForLatlong.dismiss();

                flagForAllPermission = true;
                spf.edit().putString(AppConstants.LATTITUDE, String.valueOf(latitude)).apply();
                spf.edit().putString(AppConstants.LONGITUDE, String.valueOf(longitude)).apply();

                try {
                    MapsInitializer.initialize(getActivity().getApplicationContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // mMapView.onCreate(mBundle);
                //mMapView.onResume();
                mMapView.getMapAsync(this);

            }

            Log.d(AppConstants.TAG, "onLocationChanged ->" + latitude + "--" + longitude);
        } else {
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }
    }

    TileOverlay overlay;

    public void setMapType(GoogleMap googleMap, final SharedPreferences spf) {

        try {
            Bundle bundleAnalytics = new Bundle();

            String event_name = "";
            final int selectedMapIndex = spf.getInt(AppConstants.SELECTED_MAP_INDEX, 0);

            if (selectedMapIndex != 3) {
                try {
                    if (overlay != null) {
                        overlay.setVisible(false);
                        overlay.remove();
                    }
                } catch (Exception e) {
                }
            }

            if (selectedMapIndex == 0) {
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "googleStandard";
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            } else if (selectedMapIndex == 1) {
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "googleSatellite";
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            } else if (selectedMapIndex == 2) {
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "googleHybrid";
                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            } else if (selectedMapIndex == 3) {
                event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "openStandard";

                TileOverlayOptions options1 = new TileOverlayOptions();
                googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                options1.tileProvider(new UrlTileProvider(256, 256) {
                    @Override
                    public synchronized URL getTileUrl(int x, int y, int zoom) {
                        String styleUrl = String.format(Locale.US, AppConstants.OPEN_STREET_MAP_STANDARD_URL, zoom, x, y);
                        //String s = String.format(Locale.US,selectedMap,zoom, x, y);
                        URL url = null;
                        try {
                            url = new URL(styleUrl);
                        } catch (MalformedURLException e) {
                            throw new AssertionError(e);
                        }
                        return url;
                    }
                });
                overlay = googleMap.addTileOverlay(options1);
            }

            Log.d(AppConstants.TAG, "Event:" + event_name);
            bundleAnalytics.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleAnalytics);
        } catch (Exception e) {
        }
    }


    void getGatwayList(String type, String gid) {
        dialogForLatlong.show();
        objApi.getGatwayListDetails(tokenLogin, 1, 10, type, "", gid, AppConstants.IS_SECURED).enqueue(new Callback<GatewayMaster>() {
            @Override
            public void onResponse(Call<GatewayMaster> call, Response<GatewayMaster> response) {
                objUtils.dismissProgressDialog(dialogForLatlong);
                try {
                    if (response.code() == 200) {

                        GatewayMaster count = response.body();
                        com.cl.lg.pojo.GatewayDetails.Data objData = count.getData();

                        GatewayDescription inventory = new GatewayDescription();
                        Bundle objBundle = new Bundle();
                        objBundle.putString(AppConstants.APN, objData.getList().get(0).getDetails().getApnNo());
                        objBundle.putString(AppConstants.MOBILE_NUMBER, objData.getList().get(0).getDetails().getMobileNumber());
                        objBundle.putString(AppConstants.IP, objData.getList().get(0).getDetails().getIpAddress());
                        objBundle.putString(AppConstants.PORT, objData.getList().get(0).getDetails().getPortAddress());
                        objBundle.putString(AppConstants.ExtendedPan, objData.getList().get(0).getDetails().getExtendedPAN());
                        objBundle.putString(AppConstants.SHORT_PAN_ID, objData.getList().get(0).getDetails().getShortPAN());
                        objBundle.putString(AppConstants.CHANNEL, objData.getList().get(0).getDetails().getScanChannel());
                        objBundle.putString(AppConstants.LATTITUDE, objData.getList().get(0).getLatitude());
                        objBundle.putString(AppConstants.LONGITUDE, objData.getList().get(0).getLongitude());
                        objBundle.putString(AppConstants.GATWAY_TYPE, type);
                        objBundle.putBoolean(AppConstants.ISFROMMAP, true);
                        inventory.setArguments(objBundle);

                        //objUtils.loadFragment(inventory, getActivity());
                        try {
                            if (getActivity() != null) {
                                FragmentManager fm = getActivity().getFragmentManager();
                                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                                fragmentTransaction.add(R.id.frm1, inventory);
                                fragmentTransaction.hide(MapFragment.this);
                                fragmentTransaction.addToBackStack(MapFragment.class.getName());
                                fragmentTransaction.commit();
                            }
                        } catch (Exception e) {}

                    } else
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());

                } catch (Exception e) {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
                }
            }

            @Override
            public void onFailure(Call<GatewayMaster> call, Throwable t) {
                objUtils.dismissProgressDialog(dialogForLatlong);
                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
            }
        });
    }

    void getDataStatus(long id) {
        dialogForLatlong.show();
        if (Utils.isInternetAvailable(getActivity())) {

            String blank = "";
            dialogForLatlong.show();
            String jsonfinal = null;

            if (lat == 0.0)
                latStr = "";
            else
                latStr = String.format("%.4f", lat);

            if (lng == 0.0)
                lngStr = "";
            else
                lngStr = String.format("%.4f", lng);

            try {
                String jsonStr = "{" +
                        "\"gId\":\"" + "" + "\"," +
                        "\"slcId\":\"" + id + "\"," +
                        "\"slcGroup\":\"" + "" + "\"," +
                        "\"powerPara\":\"" + "" + "\"," +
                        "\"statusPara\":\"" + "" + "\"," +
                        "\"Distance\":\"" + "" + "\"," +
                        "\"Longitude\":\"" + "" + "\"," +
                        "\"Latitude\":\"" + "" + "\"," +
                        "\"NodeType\":\"" + "" + "\"," +
                        "\"LastReceivedMode\":\"" + "" + "\"" +
                        "}";

              /*  JSONObject jsonObj = new JSONObject();
                jsonObj.put("gId","");
                jsonObj.put("slcId", id);
                jsonObj.put("slcGroup","");
                jsonObj.put("powerPara", "");
                jsonObj.put("statusPara", "");
                jsonObj.put("Distance"," distance");
                jsonObj.put("Longitude", "");
                jsonObj.put("Latitude", "");*/
                //jsonObj.put("LastReceivedMode","");

                JSONObject jsonObj = new JSONObject(jsonStr);
                jsonfinal = jsonObj.toString();
                Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

            } catch (Exception e) {
            }

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

            objApi.getSLCStatusListTemp(tokenLogin, "1", "20", lampTypeId, body, AppConstants.IS_SECURED).enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    objUtils.dismissProgressDialog(dialogForLatlong);
                    try {

                        if (response.code() == 200) {
                            JsonObject jsonObject = response.body();

                            JsonObject objData = jsonObject.getAsJsonObject("data");

                            String status = String.valueOf(jsonObject.get("status").getAsString());
                            //String status=objData2.toString();

                            JsonArray objJsonArray = objData.getAsJsonArray("list");

                            final JsonObject objFilter = objData.getAsJsonObject("filterCounts");

                            if (status.equalsIgnoreCase("1")) {

                                if (objJsonArray.size() != 0) {
                                    //com.cl.lg.pojo.SLCStatus2.List objListInner = objList.get(i);
                                    StatusResponse2 objStatusResponse2 = new StatusResponse2();
                                    JsonObject objData1 = (JsonObject) objJsonArray.get(0);
                                    objStatusResponse2.setSlcNo(objData1.get("slcNo").getAsString());
                                    objStatusResponse2.setName(objData1.get("name").getAsString());

                                    try {
                                        objStatusResponse2.setDateTime(objData1.get("datetime").getAsString());
                                    } catch (Exception e) {
                                        objStatusResponse2.setDateTime("");
                                    }

                                    try {
                                        objStatusResponse2.setD9Driver("0");
                                        objStatusResponse2.setLatitude(objData1.get("latitude").getAsString());
                                        objStatusResponse2.setLongitude(objData1.get("longitude").getAsString());
                                    } catch (Exception e) {
                                        objStatusResponse2.setLatitude("");
                                        objStatusResponse2.setLongitude("");
                                    }

                                    try {
                                        objStatusResponse2.setLastCommunicatedOn(objData1.get("lastCommunicatedOn").getAsString());
                                    } catch (Exception e) {
                                        objStatusResponse2.setLastCommunicatedOn("");
                                    }

                                    //objStatusResponse2.setD9Driver(objData1.get("d9@Driver").getAsString());

                                    objStatusResponse2.setLlId(R.layout.fragment_map);
                                    Gson objGson = new Gson();
                                    String jsonInner = objGson.toJson(objJsonArray.get(0));
                                    objStatusResponse2.setJsonString(jsonInner);

                                    if (!objStatusResponse2.getLastCommunicatedOn().isEmpty()) {
                                        SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();
                                        Bundle objBundle = new Bundle();
                                        objBundle.putString(AppConstants.LATTITUDE, objStatusResponse2.getLatitude());
                                        objBundle.putString(AppConstants.LONGITUDE, objStatusResponse2.getLongitude());
                                        objBundle.putString(AppConstants.SLC_NO, objStatusResponse2.getSlcNo());
                                        objBundle.putString(AppConstants.JSON_STRING, objStatusResponse2.getJsonString());
                                        objBundle.putBoolean(AppConstants.ISFROMSTATUS, true);
                                        objBundle.putInt(AppConstants.UI_ID, R.layout.fragment_map);
                                        objBundle.putString(AppConstants.UI_ID_status, "MAP");
                                        objBundle.putBoolean(AppConstants.IS_FROM_DASHBOARD, false);
                                        objBundle.putString(AppConstants.MODE_TEXT, "");
                                        objBundle.putInt(AppConstants.IS_SEARCH_ON, 0);
                                        objBundle.putBoolean(AppConstants.IS_NEAR_ME, false);

                                        fragment.setArguments(objBundle);

                                        FragmentManager fm = getActivity().getFragmentManager();
                                        FragmentTransaction fragmentTransaction = fm.beginTransaction();
                                        fragmentTransaction.add(R.id.frm1, fragment);
                                        fragmentTransaction.hide(MapFragment.this);
                                        fragmentTransaction.addToBackStack(MapFragment.class.getName());
                                        fragmentTransaction.commit();

                                        //objUtils.loadFragment(fragment, getActivity());
                                    }

                                } else
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.no_data_found), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    objUtils.dismissProgressDialog(dialogForLatlong);
                }
            });
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }
    }

    void getParamters(String lampTypeId) {
        dialogForLatlong.show();
        objApi.getParametersApi(tokenLogin, lampTypeId, AppConstants.IS_SECURED).enqueue(new Callback<ParametersMasters>() {
            @Override
            public void onResponse(Call<ParametersMasters> call, Response<ParametersMasters> response) {

                Log.i(AppConstants.TAG, "---*-");
                if (response.code() == 200) {
                    Log.i(AppConstants.TAG, "---*-");
                    ParametersMasters objParametersMasters = response.body();
                    java.util.List<com.cl.lg.pojo.Parameters.Datum> objData = objParametersMasters.getData();

                    if (objParametersMasters.getStatus().equalsIgnoreCase("1")) {
                        try {
                            Log.i(AppConstants.TAG, "---*-");
                            //Log.i(AppConstants.TAG, "parameters:" + objData.getParameters().toString());
                            Utils.SaveArraylistInSPF(spf, objData);
                        } catch (Exception e) {
                            Utils.SaveArraylistInSPF(spf, new ArrayList<>());
                            Log.i(AppConstants.TAG, "EEEE");
                        }

                        getData(minX, minY, maxX, maxY, true);
                    } else
                        objUtils.dismissProgressDialog(dialogForLatlong);
                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    objUtils.dismissProgressDialog(dialogForLatlong);
                }
            }

            @Override
            public void onFailure(Call<ParametersMasters> call, Throwable t) {
                objUtils.dismissProgressDialog(dialogForLatlong);
                Log.i(AppConstants.TAG, "Fail");
            }
        });
    }

}
    /* BitmapDescriptor descriptor;
                                    String type = "";
                                    if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_COMM_FAULT)) {
                                        type=getResources().getString(R.string.comm_fault);
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.comm_map_fault, marker_image));
                                    } else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_OFF)) {
                                        type=getResources().getString(R.string.lamp_off);
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.lampoff_map, marker_image));
                                    } else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_ON)) {
                                        type=getResources().getString(R.string.lamp_on);
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.lampon_map, marker_image));
                                    } else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_LAMPS_DIMMED)) {
                                        type=getResources().getString(R.string.lamp_dim);
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.lampdim_map, marker_image));
                                    } else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_GATEWAYS_CONNECTED)) {
                                        type=getResources().getString(R.string.g_connected);
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.connected_map, marker_image));
                                    } else if (objList.get(i).getG().equalsIgnoreCase(AppConstants.MAP_GATEWAY_DISCONNECTED)) {
                                        type=getResources().getString(R.string.g_connected);
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.disconnected_map, marker_image));
                                    } else
                                        descriptor = BitmapDescriptorFactory.fromBitmap(new Utils().getMarkerBitmapFromView(mCustomMarkerView, R.drawable.custome_pin, marker_image));

                                    googleMapGlobal.addMarker(new MarkerOptions()
                                            .position(new LatLng(objList.get(i).getC(), objList.get(i).getD()))
                                            .title(type)
                                            .snippet(objList.get(i).getF())
                                            .icon(descriptor));*/






     /*   @Override
        public void onInfoWindowClick(Marker marker) {
            Log.d(AppConstants.TAG, "----");
            Datum obj= (Datum) marker.getTag();

            *//*if(obj instanceof testCluster){
                testCluster cluster= (testCluster) marker.getTag();
                cluster.getMarkerCollection().getMarkers();
                Log.d(AppConstants.TAG, "----testCluster");
            }

            if(obj instanceof Datum){
                Log.d(AppConstants.TAG, "----Datum");
            }*//*

            //working but
            *//*if (isPoleDetailVisibility.equalsIgnoreCase("Yes")) {
                String text[]=marker.getTitle().split("#");
                String id=text[1];
               // com.CL.slcscanner.Pojo.ListResponse.List mData = (com.CL.slcscanner.Pojo.ListResponse.List) marker.getTag();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                PoleDataDisplayFragment fragement = new PoleDataDisplayFragment();

                Bundle mBundle = new Bundle();
                //mBundle.putSerializable("DATA_FOR_DISPLAY", mData);
                mBundle.putString("ID",id);//mData.getID().toString()
                mBundle.putBoolean("IS_FROM_MAP", true);
                fragement.setArguments(mBundle);
                fragement.setArguments(mBundle);
                fragmentTransaction.replace(R.id.frm1, fragement);
                fragmentTransaction.commit(); // save the changes
            }*//*
        }
*/
       /* @Override
        public boolean onMarkerClick(Marker marker) {
            Log.d(AppConstants.TAG, "----007--->" + marker.getTitle());
            marker.getTitle();
            *//*com.CL.slcscanner.Pojo.ListResponse.List mData = (com.CL.slcscanner.Pojo.ListResponse.List) marker.getTag();
            if (mData.getTag().equalsIgnoreCase("cluster")) {
                objDatum = mData;
            }*//*
            Object obj=marker.getTag();

            String snipet=marker.getSnippet();
            String title=marker.getTitle();

            if(obj instanceof testCluster) {
                testCluster cluster = (testCluster) marker.getTag();
                cluster.getMarkerCollection().getMarkers();
                Log.d(AppConstants.TAG, "----testCluster");

            }

            if(obj instanceof Datum){
                Log.d(AppConstants.TAG, "----Datum");
            }

            Log.d(AppConstants.TAG, "----007--->");
            return false;
        }*/