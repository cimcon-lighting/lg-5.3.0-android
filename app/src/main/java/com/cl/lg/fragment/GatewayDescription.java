package com.cl.lg.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.networking.API;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Locale;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GatewayDescription extends Fragment implements View.OnClickListener {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvOk)
    TextView tvOK;

    @BindView(R.id.tvGetDirection)
    TextView tvGetDirection;

    @BindView(R.id.tvApn)
    TextView tvApn;
    @BindView(R.id.tvUserId)
    TextView tvUserId;
    @BindView(R.id.tvMobile)
    TextView tvMobile;
    @BindView(R.id.tvIP)
    TextView tvIP;
    @BindView(R.id.tvPort)
    TextView tvPort;

    @BindView(R.id.tvExtendedPAN)
    TextView tvExtendedPAN;
    @BindView(R.id.tvShortPanID)
    TextView tvShortPanID;
    @BindView(R.id.tvChannelNo)
    TextView tvChannelNo;
    @BindView(R.id.tvLat)
    TextView tvLat;
    @BindView(R.id.tvLng)
    TextView tvLng;

    Utils objUtils;
    View view;

    API objApi;
    String token;

    ProgressDialog dialog_wait;
    SharedPreferences spf;
    String type = "";
    Bundle objBundle;

    String currentLat;
    String currentLong;
    boolean isFromMap=false;

    private  String GatwayLat;
    private  String GatwayLng;
    private FirebaseAnalytics mFirebaseAnalytics;

    int searchvisibility=View.GONE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gateway_description, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();

        spf=getActivity().getSharedPreferences(AppConstants.SPF,Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(),"gatewayDetails", null /* class override */);

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        if (getArguments() != null) {
            objBundle = getArguments();
            type = objBundle.getString(AppConstants.GATWAY_TYPE);
            tvApn.setText(objBundle.getString(AppConstants.APN));
            tvMobile.setText(objBundle.getString(AppConstants.MOBILE_NUMBER));
            tvIP.setText(objBundle.getString(AppConstants.IP));
            tvPort.setText(objBundle.getString(AppConstants.PORT));
            tvExtendedPAN.setText(objBundle.getString(AppConstants.ExtendedPan));
            tvShortPanID.setText(objBundle.getString(AppConstants.SHORT_PAN_ID));
            tvChannelNo.setText(objBundle.getString(AppConstants.CHANNEL));
            GatwayLat=objBundle.getString(AppConstants.LATTITUDE);
            GatwayLng=objBundle.getString(AppConstants.LONGITUDE);
            tvLat.setText(GatwayLat);
            tvLng.setText(GatwayLng);
            isFromMap=objBundle.getBoolean(AppConstants.ISFROMMAP,false);
            searchvisibility=objBundle.getInt(AppConstants.IS_SEARCH_ON,View.GONE);
        }

        tvOK.setOnClickListener(this);
        tvGetDirection.setOnClickListener(this);
        ivBack.setOnClickListener(this);


        currentLat=spf.getString(AppConstants.LATTITUDE,"");
        currentLong=spf.getString(AppConstants.LONGITUDE,"");

        if(isFromMap){
            ((MainActivity)getActivity()).selectDashboard(false);
            ((MainActivity)getActivity()).selectStatus(false);
            ((MainActivity)getActivity()).selectMap(true);
            ((MainActivity)getActivity()).selectMenu(false);
        }else{
            ((MainActivity)getActivity()).selectDashboard(false);
            ((MainActivity)getActivity()).selectStatus(false);
            ((MainActivity)getActivity()).selectMap(false);
            ((MainActivity)getActivity()).selectMenu(true);
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
            case R.id.tvOk:
                //navigate(type);
                getActivity().onBackPressed();
                break;
            case R.id.tvGetDirection:

                //navigationRouteMapBox();

                if(!GatwayLng.toString().equalsIgnoreCase("")&& !GatwayLat.toString().equals("")) {
                    if(objUtils.isGoogleMapsInstalled(getActivity())) {
                        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%s,%s", GatwayLat, GatwayLng);
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setPackage("com.google.android.apps.maps");
                        startActivity(intent);
                    }else{
                        objUtils.dialogForNavigatePlayStore(getActivity(), getResources().getString(R.string.google_map_not_installed), "com.google.android.apps.maps");
                    }
                }else{
                    Utils.dialogForMessage(getActivity(),getString(R.string.gatway_location_not_foud));
                }
                break;
        }
    }

   /* void navigationRouteMapBox() {
        String sortedLatLng=GatwayLat+ "," + GatwayLng+ "#";
        Bundle objBundle = new Bundle();
        Log.i(AppConstants.TAG, sortedLatLng);
        objBundle.putInt(AppConstants.SIZE_ROUTE, 1);
        objBundle.putString(AppConstants.SOURTED_ROUTE, sortedLatLng);
        RouteFragment objFragment=new RouteFragment();
        objFragment.setArguments(objBundle);

        objUtils.loadFragment(objFragment, getActivity());
    }*/


    void navigate(String type1) {
        if(isFromMap){
            objUtils.loadFragment(new MapFragment(), getActivity());
        }else {
            GatewayInventory inventory = new GatewayInventory();
            Bundle objBundle = new Bundle();
            objBundle.putString(AppConstants.GATWAY_TYPE, type1);
            objBundle.putInt(AppConstants.IS_SEARCH_ON,searchvisibility);
            inventory.setArguments(objBundle);
            objUtils.loadFragment(inventory, getActivity());
        }
    }
}