package com.cl.lg.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cl.lg.pojo.CommonDialogResponse;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBSLX.db";

    public static final String CMD_TABLE_NAME = "table_cmd";

    public static final String STATUS_TABLE_NAME = "table_status";
    public static final String GATWAY_TABLE_NAME = "table_gatway";


    //field details of SLCImport
    public static final String CMD_ID = "CmdId";
    public static final String CMD_NAME = "CmdName";

    public static final String STATUS_ID = "StatusId";
    public static final String STATUS_NAME = "StatusName";

    public static final String GATWAY_ID = "GId";
    public static final String GATWAY_NAME = "GName";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "create table " + CMD_TABLE_NAME +
                        "(" + CMD_ID + " text," +
                        CMD_NAME + " text)"
        );

        db.execSQL(
                "create table " + STATUS_TABLE_NAME +
                        "(" + STATUS_ID + " text," +
                        STATUS_NAME + " text)"
        );

        db.execSQL(
                "create table " + GATWAY_TABLE_NAME +
                        "(" + GATWAY_ID + " text," +
                        GATWAY_NAME + " text)"
        );
    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS " + CMD_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + STATUS_TABLE_NAME);
        database.execSQL("DROP TABLE IF EXISTS " + GATWAY_TABLE_NAME);
        onCreate(database);
    }

    public boolean insertcmd(Integer ID, String slcId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CMD_ID, ID);
        contentValues.put(CMD_NAME, slcId);

        db.insertWithOnConflict(CMD_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

        return true;
    }

    public boolean insertStatus(String ID, String slcId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STATUS_ID, ID);
        contentValues.put(STATUS_NAME, slcId);

        db.insertWithOnConflict(STATUS_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);

        return true;
    }

    public boolean insertGatway(Integer ID, String slcId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(GATWAY_ID, ID);
        contentValues.put(GATWAY_NAME, slcId);
        db.insertWithOnConflict(GATWAY_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CMD_TABLE_NAME + " where id=" + id + "", null);
        return res;
    }

    public void deleteTableData(String tblName) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(tblName, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<CommonDialogResponse> getAllCmd() {
        ArrayList<CommonDialogResponse> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + CMD_TABLE_NAME, null);

        res.moveToFirst();
        while (res.isAfterLast() == false) {
            CommonDialogResponse objSlcsBean = new CommonDialogResponse();

            objSlcsBean.setValue(res.getString(res.getColumnIndex(CMD_NAME)));
            objSlcsBean.setId(res.getInt(res.getColumnIndex(CMD_ID)));

            array_list.add(objSlcsBean);

            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<CommonDialogResponse> getAllStatus() {
        ArrayList<CommonDialogResponse> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + STATUS_TABLE_NAME, null);

        res.moveToFirst();
        while (res.isAfterLast() == false) {
            CommonDialogResponse objSlcsBean = new CommonDialogResponse();

            objSlcsBean.setValue(res.getString(res.getColumnIndex(STATUS_NAME)));
            objSlcsBean.setId(res.getInt(res.getColumnIndex(STATUS_ID)));

            array_list.add(objSlcsBean);

            res.moveToNext();
        }
        return array_list;
    }

    public ArrayList<CommonDialogResponse> getAllGatway() {
        ArrayList<CommonDialogResponse> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + GATWAY_TABLE_NAME, null);

        res.moveToFirst();
        while (res.isAfterLast() == false) {
            CommonDialogResponse objSlcsBean = new CommonDialogResponse();

            objSlcsBean.setValue(res.getString(res.getColumnIndex(GATWAY_NAME)));
            objSlcsBean.setId(res.getInt(res.getColumnIndex(GATWAY_ID)));
            objSlcsBean.setTempId(String.valueOf(res.getInt(res.getColumnIndex(GATWAY_ID))));
            objSlcsBean.setType("SLC_GATWAY");
            array_list.add(objSlcsBean);

            res.moveToNext();
        }
        return array_list;
    }


}