package com.cl.lg.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.widget.SearchView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.MainActivity;
import com.cl.lg.adapters.ListDialogSentCommandAdapter;
import com.cl.lg.adapters.SLCServiceDayburnerDetailsAdapter;
import com.cl.lg.adapters.SLCServiceDetailsAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;
import com.cl.lg.pojo.Daybuner.DayburnerOutages;
import com.cl.lg.pojo.FaultySLCMaster.Data;
import com.cl.lg.pojo.FaultySLCMaster.SLCFaulty;
import com.cl.lg.pojo.FaultySLCMaster.SlcList;
import com.cl.lg.pojo.SLCName.SLCName;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardSLCServiceDetailsFragment extends Fragment
        implements SLCServiceDetailsAdapter.MyCallbackForControl, SLCServiceDayburnerDetailsAdapter.MyCallbackForControl, View.OnClickListener {

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.btnRefresh)
    ImageView btnRefresh;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.tbTitleLine1)
    TextView tbTitleLine1;

    @BindView(R.id.rvStatus)
    RecyclerView rvStatus;

    @BindView(R.id.tvDetailDate)
    TextView tvDetailDate;

    @BindView(R.id.tvSLC)
    TextView tvSLC;

    @BindView(R.id.tvDetailAddress)
    TextView tvDetailAddress;

    @BindView(R.id.llSlcCommands)
    LinearLayout llSlcCommands;

    @BindView(R.id.llstartendDate)
    LinearLayout llstartendDate;

    @BindView(R.id.edtStartDate)
    EditText edtStartDate;

    @BindView(R.id.edtEndDate)
    EditText edtEndDate;

    @BindView(R.id.ivEmail)
    ImageView ivEmail;

    @BindView(R.id.ivRoute)
    ImageView ivRoute;

    @BindView(R.id.cbSelectAll)
    CheckBox cbSelectAll;

    @BindView(R.id.edtSLCName)
    TextInputEditText edtSLCName;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    @BindView(R.id.btnClear)
    Button btnClear;

    java.util.List<SlcList> mList;
    SLCServiceDetailsAdapter adapter;
    SLCServiceDayburnerDetailsAdapter adapterDayBurner;
    EndlessRecyclerViewScrollListener scrollListener;

    View view;

    ArrayList<CommonDialogResponse> mListDialog;
    ArrayList<CommonDialogResponse> filterList;

    ArrayList<com.cl.lg.pojo.SLCStatus2.List> mFinalStatusList;
    Utils objUtils;

    Bundle objBundle;
    int neverComm = 0;

    ProgressDialog dialog_wait;

    API objApi;
    String token;
    SharedPreferences spf;
    int totalcount = 0;

    ArrayList<SlcList> mListFinal2;
    ArrayList<com.cl.lg.pojo.Daybuner.List> mListFinalDayburner;
    ArrayList<CommonResponseStatusDetailsDialog> mListDialogStatus;
    boolean flag = false;

    int param;
    String title;
    private FirebaseAnalytics mFirebaseAnalytics;

    boolean isFromCommunication = false;
    boolean isFromDayBurner = false;
    Calendar myCalendar;

    String slc_id;
    final ArrayList<CommonDialogResponse> mListSearch = new ArrayList<>();

    String myFormat;
    SimpleDateFormat sdf;
    Calendar myCalendarFrom, myCalendarTo;

    String lamptypid;
    String nodeTypeid;
    boolean isSingleNodeType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragement_dashboard_details, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "slclist", null /* class override */);

        //Glide.with(this).load(R.drawable.bg).into(ivBgStatus);
        objUtils = new Utils();
        filterList = new ArrayList<>();
        isSingleNodeType=spf.getBoolean(AppConstants.isSingleNodeType,false);

        myFormat = "dd/MM/yyyy"; //In which you need put here
        sdf = new SimpleDateFormat(myFormat, Locale.US);

        objBundle = getArguments();
        objApi = new LG().networkCall(getActivity(), false);

        token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        mListDialogStatus = new ArrayList<>();
        mListFinal2 = new ArrayList<>();
        mListFinalDayburner = new ArrayList<>();

        lamptypid=spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID,"");
        btnClear.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        if (objBundle != null) {
            title = objBundle.getString(AppConstants.TITLE, "");
            param = objBundle.getInt(AppConstants.SLC_SERVICE_PARAMS, 0);

            if (param == 2)
                isFromCommunication = true;
        }

        tbTitleLine1.setText(title);
        mList = new ArrayList<>();
        mFinalStatusList = new ArrayList<>();

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        mListDialog = new ArrayList<>();
        rvStatus.setHasFixedSize(true);
        rvStatus.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvStatus.setLayoutManager(linearLayoutManager);

        rvStatus.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        cbSelectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    for (int i = 0; i < mListFinalDayburner.size(); i++) {
                        mListFinalDayburner.get(i).setChecked(true);
                    }
                } else {
                    for (int i = 0; i < mListFinalDayburner.size(); i++) {
                        mListFinalDayburner.get(i).setChecked(false);
                    }
                }
                adapterDayBurner.notifyDataSetChanged();
            }
        });

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list
                    Log.d(AppConstants.TAG, "scroll listner call");
                    if (Utils.isInternetAvailable(getActivity())) {

                    if (param == 4 || param == 5) {
                        if (mListFinalDayburner.size() < totalcount) {
                            Log.i(AppConstants.TAG, "Pg:" + page);
                            getDayburnerOutages(page + 1);
                        }
                    } else {
                        if (mListFinal2.size() < totalcount) {
                            Log.i(AppConstants.TAG, "Pg:" + page);
                            getFaultySLCData(page + 1);
                        }
                    }
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                //dummyData();
            }
        };

        rvStatus.addOnScrollListener(scrollListener);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objUtils.loadFragment(new SLCServiceGraphFramgment(), getActivity());
            }
        });

        if (param == 5 || param == 4) {
            tvDetailDate.setText(getResources().getString(R.string.date).toUpperCase());
            isFromDayBurner = true;
            tvSLC.setVisibility(View.GONE);
            tvDetailAddress.setVisibility(View.VISIBLE);
            tvSLC.setVisibility(View.GONE);
            tvDetailAddress.setText(getResources().getString(R.string.address).toUpperCase());

            cbSelectAll.setVisibility(View.VISIBLE);
            ivEmail.setVisibility(View.VISIBLE);
            ivRoute.setVisibility(View.VISIBLE);

            llstartendDate.setVisibility(View.VISIBLE);
            ivFilter.setOnClickListener(this);
            adapterDayBurner = new SLCServiceDayburnerDetailsAdapter(getActivity(), mListFinalDayburner, this);
            rvStatus.setAdapter(adapterDayBurner);
            getDayburnerOutages(1);

        } else {
            tvDetailAddress.setVisibility(View.GONE);
            tvSLC.setVisibility(View.VISIBLE);
            tvDetailAddress.setVisibility(View.GONE);
            tvSLC.setVisibility(View.VISIBLE);

            ivEmail.setVisibility(View.GONE);
            ivRoute.setVisibility(View.GONE);

            cbSelectAll.setVisibility(View.GONE);
            adapter = new SLCServiceDetailsAdapter(getActivity(), mListFinal2, this, true, isFromDayBurner);
            rvStatus.setAdapter(adapter);
            llstartendDate.setVisibility(View.GONE);
            getFaultySLCData(1);
        }

        swpRefresshPole.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        swpRefresshPole.setRefreshing(false);
                    }
                }, 2500);

                if (param == 4 || param == 5) {
                    edtEndDate.setText("");
                    edtStartDate.setText("");
                    edtSLCName.setText("");

                    defualtDates();
                    mListFinalDayburner.clear();
                    adapterDayBurner.notifyDataSetChanged();
                    getDayburnerOutages(1);

                } else {
                    mFinalStatusList.clear();
                    adapter.notifyDataSetChanged();
                    getFaultySLCData(1);
                }
            }
        });

        edtStartDate.setOnClickListener(this);
        edtEndDate.setOnClickListener(this);
        edtSLCName.setOnClickListener(this);

        defualtDates();

        ((MainActivity) getActivity()).selectDashboard(true);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(false);
    }

    void defualtDates() {
        //logic
        // now 21-1-2020 so startdate 19-1-2020 enddate: 20-1-2020
        //Default:
        myCalendarFrom = Calendar.getInstance();
        myCalendarFrom.add(Calendar.DATE, -2);
        String startDate = sdf.format(myCalendarFrom.getTime()); // 2 day before from now
        edtStartDate.setText(startDate);

        myCalendarTo = Calendar.getInstance();
        myCalendarTo.add(Calendar.DATE, -1);
        String endDate = sdf.format(myCalendarTo.getTime()); // 1 day before from now
        edtEndDate.setText(endDate);
        Log.i(AppConstants.TAG, "Start Date: " + startDate + "Default EndDate: " + endDate);
    }

    DatePickerDialog.OnDateSetListener startdate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendarFrom = Calendar.getInstance();
            myCalendarFrom.set(Calendar.YEAR, year);
            myCalendarFrom.set(Calendar.MONTH, monthOfYear);
            myCalendarFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtStartDate.setText(sdf.format(myCalendarFrom.getTime()));
        }
    };

    DatePickerDialog.OnDateSetListener enddate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendarTo = Calendar.getInstance();
            myCalendarTo.set(Calendar.YEAR, year);
            myCalendarTo.set(Calendar.MONTH, monthOfYear);
            myCalendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtEndDate.setText(sdf.format(myCalendarTo.getTime()));
        }
    };

    void dialog_search_list(final TextInputEditText editText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButtondir = dialog.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);

        objRecyclerView.setItemViewCacheSize(20);
        objRecyclerView.setDrawingCacheEnabled(true);
        objRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSentCommandAdapter adapterDialog = new ListDialogSentCommandAdapter(getActivity(), mListSearch,false, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String displayText = response.getValue().toString();
                editText.setText(displayText);

                slc_id = String.valueOf(response.getId());
                spf.edit().putString(AppConstants.SLC_ID_NAV, slc_id).apply();
                spf.edit().putString(AppConstants.SLC_ID_VALUE_NAVE, displayText).apply();

                dialog.dismiss();
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        getSLCName("", adapterDialog);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        objSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.toString().trim().equalsIgnoreCase("")) {
                    getSLCName("", adapterDialog);
                } else {
                    getSLCName(s.toString(), adapterDialog);
                }
                return false;
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void getSLCName(String slc, final ListDialogSentCommandAdapter adapter) {
        //dialog_wait.show();
        mListSearch.clear();
        lamptypid=spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID,"");
        if (isSingleNodeType)
            nodeTypeid = "";
        else
            nodeTypeid = spf.getString(AppConstants.SELECTED_NODE_TYPE_ID, "");


        objApi.getSLCName(token, slc, "",lamptypid,AppConstants.IS_SECURED,"").enqueue(new Callback<SLCName>() {
            @Override
            public void onResponse(Call<SLCName> call, Response<SLCName> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    SLCName slcName = response.body();
                    if (slcName.getStatus().equalsIgnoreCase("1")) {
                        for (int i = 0; i < slcName.getData().size(); i++) {
                            CommonDialogResponse response1 = new CommonDialogResponse();
                            //response1.setValue(slcName.getData().get(i).getText());
                            response1.setValue(slcName.getData().get(i).getValue());
                            response1.setId_str(slcName.getData().get(i).getValue());
                            response1.setType("SLC_NAME");
                            mListSearch.add(response1);
                        }
                        adapter.notifyDataSetChanged();
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<SLCName> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    @Override
    public void onClickStatusUI(int position, SlcList objListDetail) {

        String jsonString = "{" +
                "d8@communication:0}";
        Log.d(AppConstants.TAG, objListDetail.getSlcNo().toString());


            //load fragment
            FragmentManager fm = getActivity().getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();

            Bundle objBundle = new Bundle();
            objBundle.putString(AppConstants.LATTITUDE, "");
            objBundle.putString(AppConstants.LONGITUDE, "");
            objBundle.putString(AppConstants.SLC_NO, objListDetail.getSlcNo());
            objBundle.putString(AppConstants.JSON_STRING, jsonString);
            objBundle.putBoolean(AppConstants.ISFROMSTATUS, false);
            objBundle.putInt(AppConstants.UI_ID, -1);
            objBundle.putString(AppConstants.UI_ID_status, "");
            objBundle.putInt(AppConstants.SLC_SERVICE_PARAMS, param);
            fragment.setArguments(objBundle);

            fragmentTransaction.replace(R.id.frm1, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();


    }

    void getFaultySLCData(int page) {
        dialog_wait.show();
        objApi.getFaultSLCService(token, page, 15, param,lamptypid,AppConstants.IS_SECURED).enqueue(new Callback<SLCFaulty>() {
            @Override
            public void onResponse(Call<SLCFaulty> call, Response<SLCFaulty> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {

                    SLCFaulty obj = response.body();
                    if (obj.getStatus().equalsIgnoreCase("1")) {
                        Data objData = obj.getData();

                        mListFinal2.addAll(objData.getSlcList());
                        totalcount = Integer.parseInt(objData.getTotalRecords());
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SLCFaulty> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void getDayburnerOutages(int page) {

        if (page == 1) {
            mListFinalDayburner.clear();
            adapterDayBurner.notifyDataSetChanged();
        }

        String reportType = "";
        if (param == 5) {
            reportType = "DayBurn";
        } else if (param == 4) {
            reportType = "Outage";
        }

        String start;
        String end;

        start = edtStartDate.getText().toString();
        end = edtEndDate.getText().toString();

        //start = "1/1/2017";
        //end = "21/1/2020";

        dialog_wait.show();
        objApi.getDayBurnerOutages(token, reportType, page, start, end, edtSLCName.getText().toString(),lamptypid,AppConstants.IS_SECURED).enqueue(new Callback<DayburnerOutages>() {
            @Override
            public void onResponse(Call<DayburnerOutages> call, Response<DayburnerOutages> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {

                    DayburnerOutages obj = response.body();
                    if (obj.getStatus().equalsIgnoreCase("1")) {

                        com.cl.lg.pojo.Daybuner.Data objData = obj.getData();

                        if (objData.getList().size() != 0) {
                            rvStatus.setVisibility(View.VISIBLE);
                            tvNorecordsList.setVisibility(View.GONE);
                            mListFinalDayburner.addAll(objData.getList());
                            totalcount = objData.getTotalRecords();
                            adapterDayBurner.notifyDataSetChanged();

                        } else {
                            tvNorecordsList.setVisibility(View.VISIBLE);
                            tvNorecordsList.setText(getResources().getString(R.string.no_data));
                            rvStatus.setVisibility(View.GONE);
                        }
                    }
                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<DayburnerOutages> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    @Override
    public void onClickStatusUI(int position, com.cl.lg.pojo.Daybuner.List mList) {

        String status;
        if (param == 4)
            status = AppConstants.OUTEAGES;
        else
            status = AppConstants.DAY_BURNER;

        HistoryDayburnerCountFragment objFragment2 = new HistoryDayburnerCountFragment();
        Bundle objBundle2 = new Bundle();
        objBundle2.putString(AppConstants.UI_ID, AppConstants.COUNT);
        objBundle.putString(AppConstants.SLC_NO,slc_id);
        objFragment2.setArguments(objBundle2);
        objUtils.loadFragment(objFragment2, getActivity());

        FragmentManager fm = getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        SLCDetailsPowerParamFragment fragment = new SLCDetailsPowerParamFragment();

        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.LATTITUDE, "");
        objBundle.putString(AppConstants.LONGITUDE, "");
        objBundle.putString(AppConstants.SLC_NO, mList.getSlcNumber());
        objBundle.putString(AppConstants.JSON_STRING, "");
        objBundle.putBoolean(AppConstants.ISFROMSTATUS, false);
        objBundle.putInt(AppConstants.UI_ID, -1);
        objBundle.putString(AppConstants.UI_ID_status, status);
        objBundle.putInt(AppConstants.SLC_SERVICE_PARAMS, param);
        fragment.setArguments(objBundle);

        fragmentTransaction.replace(R.id.frm1, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSearch:
                getDayburnerOutages(1);
                break;
            case R.id.btnClear:

                defualtDates();
                edtSLCName.setText("");
                getDayburnerOutages(1);
                break;
            case R.id.ivFilter:

                if (llSlcCommands.getVisibility() == View.VISIBLE) {
                    llSlcCommands.setVisibility(View.GONE);
                    edtEndDate.setText("");
                    edtStartDate.setText("");
                    edtSLCName.setText("");
                    defualtDates();
                } else
                    llSlcCommands.setVisibility(View.VISIBLE);
                break;

            case R.id.edtStartDate:
                myCalendarFrom = Calendar.getInstance();
                DatePickerDialog objDatePickerDialogFrom =
                        new DatePickerDialog(getActivity(), startdate, myCalendarFrom
                                .get(Calendar.YEAR), myCalendarFrom.get(Calendar.MONTH),
                                myCalendarFrom.get(Calendar.DAY_OF_MONTH));
                objDatePickerDialogFrom.getDatePicker().setMaxDate(myCalendarFrom.getTimeInMillis());
                objDatePickerDialogFrom.show();
                break;
            case R.id.edtEndDate:
                if (!edtStartDate.getText().toString().equalsIgnoreCase("")) {
                    myCalendarTo = Calendar.getInstance();
                    DatePickerDialog objDatePickerDialog =
                            new DatePickerDialog(getActivity(), enddate, myCalendarTo
                                    .get(Calendar.YEAR), myCalendarTo.get(Calendar.MONTH),
                                    myCalendarTo.get(Calendar.DAY_OF_MONTH));
                    objDatePickerDialog.getDatePicker().setMinDate(myCalendarFrom.getTimeInMillis());
                    objDatePickerDialog.getDatePicker().setMaxDate(myCalendarTo.getTimeInMillis());
                    objDatePickerDialog.show();
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.please_choose_from_date));
                }
                break;
            case R.id.edtSLCName:
                if (Utils.isInternetAvailable(getActivity())) {
                    dialog_search_list(edtSLCName);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;
        }
    }
}