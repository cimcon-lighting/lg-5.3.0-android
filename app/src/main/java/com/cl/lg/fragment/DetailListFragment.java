package com.cl.lg.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.adapters.StatusListDetailsAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.SentCmdDetails.SentCmdDetails;
import com.cl.lg.pojo.SentCmdDetails.TrackDetail;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailListFragment extends Fragment {

    @BindView(R.id.llBack)
    LinearLayout llBack;

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.tvGatewayanme)
    TextView tvGatewayanme;

    @BindView(R.id.tvTrackId)
    TextView tvTrackId;

    @BindView(R.id.llGatwaySent)
    LinearLayout llGatwaySent;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    String slc_no;

    API objApi;
    View view;
    String clientCommType;

    ArrayList<TrackDetail> mListDetails;
    RecyclerView rvDetailsSendtCommand;
    Bundle mBundle;

    ProgressDialog objProgressDialog;
    StatusListDetailsAdapter mAdapter;

    Utils objUtil;
    String username, status;

    Bundle objBundle;
    String from;
    String track_id;
    String gatway_name;

    String token;
    SharedPreferences spf;

    int total_records;
    EndlessRecyclerViewScrollListener scrollListener;

    String cliet_typ;

    int SearchViewVisibility = 0;
    private FirebaseAnalytics mFirebaseAnalytics;

    String format;
    private String TrackId = "", GatwayName = "", FromDate = "", ToDate = "", Commanddname = "", cmd_id = "", status_id = "", status_sent = "";
    boolean isZigbeeContains;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_details, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objApi = new LG().networkCall(getActivity(), false);
        objUtil = new Utils();
        mBundle = getArguments();

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "sentCommandDetails", null /* class override */);

        objProgressDialog = new ProgressDialog(getActivity());
        objProgressDialog.setMessage(getResources().getString(R.string.loading));
        objProgressDialog.setCancelable(false);

        mListDetails = new ArrayList<>();


        token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        format = spf.getString(AppConstants.DATE_FORMAT, "");

        mBundle = getArguments();
        if (mBundle != null) {
            from = mBundle.getString("From", "");
            track_id = mBundle.getString("track_id");
            gatway_name = mBundle.getString("gateway_name");
            clientCommType=mBundle.getString("clientCommType");

          /*  TrackId = mBundle.getString(AppConstants.SENT_C_TRACK_ID_VALUE, "");
            GatwayName = mBundle.getString(AppConstants.SENT_C_GATWAY_VALUE, "");
            FromDate = mBundle.getString(AppConstants.SENT_C_FROM_DATE, "");
            ToDate = mBundle.getString(AppConstants.SENT_C_TO_DATE_VALE, "");

            Commanddname = mBundle.getString(AppConstants.SENT_C_CMD_NAME_VALUE, "");
            cmd_id = mBundle.getString(AppConstants.SENT_C_CMD_NAME_ID, "");
            status_id = mBundle.getString(AppConstants.SENT_C_STATUS_ID, "");
            status_sent = mBundle.getString(AppConstants.SENT_C_STATUS_VALUE, "");*/

            //SearchViewVisibility = mBundle.getInt(AppConstants.IS_SEARCH_ON_SENT_CMD, View.GONE);
        }
        tvGatewayanme.setText(gatway_name);
        tvTrackId.setText(track_id);

        cliet_typ = spf.getString(AppConstants.CLIENT_TYPE, "");
        isZigbeeContains=spf.getBoolean(AppConstants.isZigbeeContains,false);

        //1 Zigbee, 2 Cisco, 3 IngenuRPMA , 4 IngenuOSDI , 5 NBIOT, 6LoRaSenRa
        if (clientCommType.equalsIgnoreCase("1")) {
            llGatwaySent.setVisibility(View.VISIBLE);
        } else {
            llGatwaySent.setVisibility(View.GONE);
        }

        mAdapter = new StatusListDetailsAdapter(getActivity(), mListDetails, format);
        rvDetailsSendtCommand = view.findViewById(R.id.rvDetailsSendtCommand);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvDetailsSendtCommand.setLayoutManager(linearLayoutManager);
        rvDetailsSendtCommand.setAdapter(mAdapter);
        rvDetailsSendtCommand.addOnScrollListener(
                new EndlessRecyclerViewScrollListener(linearLayoutManager) {
                    @Override
                    public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                        if (Utils.isInternetAvailable(getActivity())) {
                            if (mListDetails.size() < total_records) {
                                Log.i(AppConstants.TAG, "Pg:" + page);
                                getSetnCommandDetails(page + 1);
                            }
                        } else {
                            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                        }
                    }
                });

        rvDetailsSendtCommand.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* if (from.toString().equalsIgnoreCase("SLCCommand"))
                    objUtil.loadFragment(new SLCCommandsFragment(), getActivity());
                else if (from.toString().equalsIgnoreCase("SentCommand"))
                    objUtil.loadFragment(new SentCommandFragment(), getActivity());
                else
                    objUtil.loadFragment(new SentCommandFragment(), getActivity());*/

                //navigateSendCommand();

                getActivity().onBackPressed();
            }
        });

        swpRefresshPole.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        swpRefresshPole.setRefreshing(false);
                    }
                }, 2500);
                mListDetails.clear();
                getSetnCommandDetails(1);
            }
        });

        if (Utils.isInternetAvailable(getActivity()))
            getSetnCommandDetails(1);
        else

            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));


        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "sentCommandDetails", null);

        //dummyData();
    }


    void navigateSendCommand() {
        if (getActivity() != null) {
            FragmentManager fm = getActivity().getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();

           /* Bundle objBundle = new Bundle();
            objBundle.putString(AppConstants.SENT_C_TRACK_ID_VALUE, TrackId);
            objBundle.putString(AppConstants.SENT_C_GATWAY_VALUE, GatwayName);
            objBundle.putString(AppConstants.SENT_C_FROM_DATE, FromDate);
            objBundle.putString(AppConstants.SENT_C_TO_DATE_VALE, ToDate);

            objBundle.putString(AppConstants.SENT_C_CMD_NAME_VALUE, Commanddname);
            objBundle.putString(AppConstants.SENT_C_STATUS_VALUE, status_sent);
            objBundle.putString(AppConstants.SENT_C_CMD_NAME_ID, cmd_id);
            objBundle.putString(AppConstants.SENT_C_STATUS_ID, status_id);*/

            //objBundle.putInt(AppConstants.IS_SEARCH_ON_SENT_CMD, SearchViewVisibility);

            SentCommandFragment fragment = new SentCommandFragment();
            //fragment.setArguments(objBundle);

            //fragmentTransaction.replace(R.id.frm1, fragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();

            fragmentTransaction.add(R.id.frm1, fragment);
            fragmentTransaction.hide(DetailListFragment.this);
            fragmentTransaction.addToBackStack(SentCommandFragment.class.getName());
            fragmentTransaction.commit();
        }
    }

    ArrayList<TrackDetail> mList = new ArrayList<>();

    void getSetnCommandDetails(int pg) {

        objProgressDialog.show();

        if (pg == 1) {

        }
        objApi.getSendCmdDetails(token, "10", track_id, String.valueOf(pg), AppConstants.IS_SECURED).enqueue(new Callback<SentCmdDetails>() {
            @Override
            public void onResponse(Call<SentCmdDetails> call, Response<SentCmdDetails> response) {

                objUtil.dismissProgressDialog(objProgressDialog);
                if (response.code() == 200) {
                    SentCmdDetails objSentCmdDetails = response.body();
                    if (objSentCmdDetails.getStatus().equalsIgnoreCase("1")) {
                        int size = objSentCmdDetails.getData().getTrackDetail().size();
                        total_records = Integer.parseInt(objSentCmdDetails.getData().getTotalRecords());
                        for (int i = 0; i < size; i++) {
                            mListDetails.add(objSentCmdDetails.getData().getTrackDetail().get(i));
                        }
                        mAdapter.notifyDataSetChanged();

                    } else {
                        Utils.dialogForMessage(getActivity(), objUtil.getStringResourceByName(getActivity(), objSentCmdDetails.getMsg().toString()));
                    }
                } else
                    objUtil.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<SentCmdDetails> call, Throwable t) {
                objUtil.dismissProgressDialog(objProgressDialog);
            }
        });

    }

    void openListFragment(Activity activity, String slcStatus) {
        try {
            if (activity != null) {
                // create a FragmentManager
                android.app.FragmentManager fm = activity.getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                android.app.FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment

                SentCommandFragment fragment = new SentCommandFragment();
                Bundle objBundle = new Bundle();
                fragment.setArguments(objBundle);

                fragmentTransaction.replace(R.id.frm1, fragment);
                fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
        }
    }

   /* void dummyData() {
        mListDetails.clear();
        for (int i = 0; i < 5; i++) {
            com.cl.lg.pojo.Asset objList = new com.cl.lg.pojo.Asset();
            objList.setAssetName("Name: " + i);
            objList.setAttrKey("Key: " + i);
            mListDetails.add(objList);
        }

        // tvNorecordsList.setVisibility(View.GONE);
        // rvDetailsSendtCommand.setVisibility(View.VISIBLE);
        mAdapter.notifyDataSetChanged();
    }*/
}