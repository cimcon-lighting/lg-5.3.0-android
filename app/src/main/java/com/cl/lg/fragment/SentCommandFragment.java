package com.cl.lg.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.cl.lg.activities.MainActivity;
import com.google.android.material.textfield.TextInputEditText;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.activities.SecurityCodeActivity;
import com.cl.lg.adapters.ListDialogSentCommandAdapter;
import com.cl.lg.adapters.SentCommandAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.GetCommands.GetCommand;
import com.cl.lg.pojo.GetGatwaylist.GetGatway;
import com.cl.lg.pojo.GetStatus.GetStatus;
import com.cl.lg.pojo.SentCommand.SendCommandBodyInner;
import com.cl.lg.pojo.SentCommand.SendcommandBody;
import com.cl.lg.pojo.SentCommand.SentCommand;
import com.cl.lg.pojo.SentCommand.TrackNetworkList;
import com.cl.lg.pojo.TrackIds;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.DBHelper;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SentCommandFragment extends Fragment implements SentCommandAdapter.MyCallbackForControl, View.OnClickListener {

    @BindView(R.id.tvNorecordsList)
    TextView tvNorecordsList;

    @BindView(R.id.rvSentCommand)
    RecyclerView rvSentCommand;

    @BindView(R.id.btnRefresh)
    ImageView btnRefresh;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout swpRefresshPole;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.llSentCommands)
    LinearLayout llSentCommands;

    @BindView(R.id.edtCommanddname)
    TextInputEditText edtCommanddname;

    @BindView(R.id.edtTrackId)
    TextInputEditText edtTrackId;

    @BindView(R.id.edtStatus)
    TextInputEditText edtStatus;

    @BindView(R.id.edtGatwayName)
    TextInputEditText edtGatwayName;

    @BindView(R.id.edtFromDate)
    EditText edtFromDate;

    @BindView(R.id.edtToDate)
    EditText edtToDate;

    @BindView(R.id.btnClear)
    Button btnClear;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    TextView tvNoRecord;

    List<com.cl.lg.pojo.ListResponse.List> mList;

    List<CommonDialogResponse> mList2;

    SentCommandAdapter adapterSentCmd;

    EndlessRecyclerViewScrollListener scrollListener;

    View view;

    Utils objUtils;
    final ArrayList<CommonDialogResponse> filterList = new ArrayList<>();

    DatePickerDialog objDatePickerDialogTo;
    DatePickerDialog objDatePickerDialogFrom;

    Calendar myCalendarFrom, myCalendarTo;
    String myFormat = "MM/dd/yyyy"; //In which you need put here
    SimpleDateFormat sdf;

    API objApi;
    ProgressDialog dialog_wait;

    SharedPreferences spf;
    String token;

    ArrayList<TrackNetworkList> mListFinal;
    String cliet_typ;
    int totalRecords = 0;

    String cmd_id = "";
    String status_id = "";

    DBHelper db;
    String date_format;

    boolean isSearchOn = false;
    int SearchVisibillity = View.GONE;
    int SearchVisibillityStatus = View.GONE;

    Bundle mBundle;
    int UI_ID;
    boolean is_near_me;

    private FirebaseAnalytics mFirebaseAnalytics;
    boolean isZigbeeContains;


    String clientCommType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragement_sent_command, null);
        init();
        return view;
    }

    void init() {

        ButterKnife.bind(this, view);
        //Glide.with(this).load(R.drawable.bg).into(ivBgSentCommand);
        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        myFormat = spf.getString(AppConstants.DATE_FORMAT, "");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "sentCommand", null /* class override */);

        date_format = spf.getString(AppConstants.DATE_FORMAT, "");

        mBundle = getArguments();

        if (mBundle != null) {
            UI_ID = mBundle.getInt(AppConstants.UI_ID,0);
            SearchVisibillityStatus = mBundle.getInt(AppConstants.IS_SEARCH_ON,View.GONE);
            is_near_me = mBundle.getBoolean(AppConstants.IS_NEAR_ME,false);
            SearchVisibillity=mBundle.getInt(AppConstants.IS_SEARCH_ON_SENT_CMD,View.GONE);
            clientCommType=mBundle.getString("clientCommType","");
        }

        db = new DBHelper(getActivity());

        cliet_typ = spf.getString(AppConstants.CLIENT_TYPE, "");
        isZigbeeContains=spf.getBoolean(AppConstants.isZigbeeContains,false);

        mList2 = new ArrayList<>();
        token = spf.getString(AppConstants.ACCESS_TOKEN, "");

        objApi = new LG().networkCall(getActivity(), false);
        objUtils = new Utils();
        mList = new ArrayList<>();
        mListFinal = new ArrayList<>();

        sdf = new SimpleDateFormat(date_format, Locale.US);

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        rvSentCommand.setHasFixedSize(true);
        rvSentCommand.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvSentCommand.setLayoutManager(linearLayoutManager);
        adapterSentCmd = new SentCommandAdapter(getActivity(), mListFinal, this);

        if (isZigbeeContains) {
            edtGatwayName.setVisibility(View.VISIBLE);
        } else {
            edtGatwayName.setVisibility(View.GONE);
        }

        if (SearchVisibillity == View.GONE) {
            llSentCommands.setVisibility(View.GONE);
            resetSearchFields();
        } else {
            llSentCommands.setVisibility(View.VISIBLE);

            edtStatus.setText(spf.getString(AppConstants.SENT_C_STATUS_VALUE, ""));
            edtGatwayName.setText(spf.getString(AppConstants.SENT_C_GATWAY_VALUE, ""));
            edtTrackId.setText(spf.getString(AppConstants.SENT_C_TRACK_ID_VALUE, ""));
            edtToDate.setText(spf.getString(AppConstants.SENT_C_TO_DATE_VALE, ""));
            edtFromDate.setText(spf.getString(AppConstants.SENT_C_FROM_DATE, ""));
            edtCommanddname.setText(spf.getString(AppConstants.SENT_C_CMD_NAME_VALUE, ""));

            cmd_id = spf.getString(AppConstants.SENT_C_CMD_NAME_ID, "");
            status_id = spf.getString(AppConstants.SENT_C_STATUS_ID, "");
        }

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //dummyData();
                if (mListFinal.size() < totalRecords) {
                    Log.i(AppConstants.TAG, "Pg:" + page);
                    getData(page + 1);
                }
                Log.i(AppConstants.TAG, "Pg:" + page);
            }
        };
        rvSentCommand.addOnScrollListener(scrollListener);
        rvSentCommand.setScrollingTouchSlop(1);
        rvSentCommand.setAdapter(adapterSentCmd);

        ivFilter.setOnClickListener(this);
        edtCommanddname.setOnClickListener(this);
        edtTrackId.setOnClickListener(this);
        edtStatus.setOnClickListener(this);
        edtGatwayName.setOnClickListener(this);
        edtFromDate.setOnClickListener(this);
        edtToDate.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //objUtils.loadFragment(new StatusFragment(), getActivity());
                //objUtils.openStatus(getActivity(), UI_ID,SearchVisibillityStatus , is_near_me,View.GONE);
                getActivity().onBackPressed();
            }
        });

        //swpRefresshPole.setRefreshing(true);

        swpRefresshPole.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        swpRefresshPole.setRefreshing(false);
                    }
                }, 2500);

                rvSentCommand.getRecycledViewPool().clear();
                adapterSentCmd.notifyDataSetChanged();
                resetSearchFields();
                getData(1);
            }
        });

        //dummyData();
        if (Utils.isInternetAvailable(getActivity())) {
            //resetSearchFields();
            getData(1);
        } else {
            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
        }

        db.deleteTableData(db.CMD_TABLE_NAME);
        db.deleteTableData(db.STATUS_TABLE_NAME);
        db.deleteTableData(db.GATWAY_TABLE_NAME);


        /*((MainActivity) getActivity()).selectDashboard(false);
        ((MainActivity) getActivity()).selectStatus(true);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(false);*/
    }

    SendcommandBody getSearchValues(String cmd, String status, String from, String to, String gatway, String trackid) {

        SendcommandBody objBody = new SendcommandBody();
        SendCommandBodyInner objInner = new SendCommandBodyInner();

        objInner.setCommandName(cmd);
        objInner.setStatus(status);

        objInner.setFromDate(from);
        objInner.setToDate(to);

        objInner.setGateway(gatway);
        objInner.setTrackId(trackid);

        List<SendCommandBodyInner> list = new ArrayList<>();
        list.add(objInner);

        objBody.setSendCommandBodyInner(list);

        return objBody;
    }

    void resetSearchFields() {
        edtToDate.setText("");
        edtGatwayName.setText("");
        edtStatus.setText("");
        edtFromDate.setText("");
        edtTrackId.setText("");
        edtCommanddname.setText("");
        cmd_id = "";
        status_id = "";

        SharedPreferences.Editor editor = spf.edit();
        editor.putBoolean(AppConstants.IS_SEARCH_ON_SENT_CMD, false);
        editor.remove(AppConstants.SENT_C_CMD_NAME_VALUE);
        editor.remove(AppConstants.SENT_C_FROM_DATE);
        editor.remove(AppConstants.SENT_C_GATWAY_VALUE);
        editor.remove(AppConstants.SENT_C_STATUS_VALUE);
        editor.remove(AppConstants.SENT_C_TO_DATE_VALE);
        editor.remove(AppConstants.SENT_C_TRACK_ID_VALUE);

        editor.remove(AppConstants.SENT_C_CMD_NAME_ID);
        editor.remove(AppConstants.SENT_C_STATUS_ID);

        editor.apply();
    }

    final DatePickerDialog.OnDateSetListener fromDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendarFrom.set(Calendar.YEAR, year);
            myCalendarFrom.set(Calendar.MONTH, monthOfYear);
            myCalendarFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtFromDate.setText(sdf.format(myCalendarFrom.getTime()));

            spf.edit().putString(AppConstants.SENT_C_FROM_DATE, edtFromDate.getText().toString()).apply();
        }
    };

    final DatePickerDialog.OnDateSetListener toDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendarTo.set(Calendar.YEAR, year);
            myCalendarTo.set(Calendar.MONTH, monthOfYear);
            myCalendarTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            edtToDate.setText(sdf.format(myCalendarTo.getTime()));
            spf.edit().putString(AppConstants.SENT_C_TO_DATE_VALE, edtToDate.getText().toString()).apply();
        }
    };


    ArrayList<CommonDialogResponse> getCommands(final String token) {
        dialog_wait.show();
        filterList.clear();

        objApi.getCommannd(token, AppConstants.IS_SECURED).enqueue(new Callback<GetCommand>() {

            @Override
            public void onResponse(Call<GetCommand> call, Response<GetCommand> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    db.deleteTableData(DBHelper.CMD_NAME);
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        GetCommand objGetCommand = response.body();
                        filterList.clear();
                        for (int i = 0; i < objGetCommand.getData().size(); i++) {
                            CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                            objCommonDialogResponse.setId(objGetCommand.getData().get(i).getCommandID());
                            objCommonDialogResponse.setValue(objGetCommand.getData().get(i).getCommandName());
                            filterList.add(objCommonDialogResponse);

                            db.insertcmd(objGetCommand.getData().get(i).getCommandID(), objGetCommand.getData().get(i).getCommandName());

                        }
                        dialog_search_list(edtCommanddname, filterList);
                        //dialog_search_list(filterList, edtCommanddname);
                    } else
                        Utils.dialogForMessage(getActivity(), response.body().getMessage());

                } else {
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<GetCommand> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
        return filterList;
    }

    void getCommands(final String token, final ListDialogSentCommandAdapter adapter, final SearchView searchView, final EditText editText) {
        dialog_wait.show();
        filterList.clear();
        objApi.getCommannd(token, AppConstants.IS_SECURED).enqueue(new Callback<GetCommand>() {

            @Override
            public void onResponse(Call<GetCommand> call, Response<GetCommand> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.body() != null) {

                    if (response.code() == 401) {
                        getActivity().finish();
                        startActivity(new Intent(getActivity(), SecurityCodeActivity.class));
                    } else if (response.code() == 200) {

                        if (response.body().getStatus().equalsIgnoreCase("1")) {
                            GetCommand objGetCommand = response.body();

                            for (int i = 0; i < objGetCommand.getData().size(); i++) {
                                CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                                objCommonDialogResponse.setId(objGetCommand.getData().get(i).getCommandID());
                                objCommonDialogResponse.setValue(objGetCommand.getData().get(i).getCommandName());
                                filterList.add(objCommonDialogResponse);
                            }
                            adapter.notifyDataSetChanged();

                            //dialog_search_list(filterList, edtCommanddname);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GetCommand> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }


    void getStatus(String token) {
        dialog_wait.show();
        filterList.clear();
        objApi.getStatus(token, AppConstants.IS_SECURED).enqueue(new Callback<GetStatus>() {
            @Override
            public void onResponse(Call<GetStatus> call, Response<GetStatus> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        GetStatus objGetCommand = response.body();
                        db.deleteTableData(DBHelper.STATUS_TABLE_NAME);
                        for (int i = 0; i < objGetCommand.getData().size(); i++) {
                            CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                            objCommonDialogResponse.setId(Integer.valueOf(objGetCommand.getData().get(i).getValue()));
                            objCommonDialogResponse.setValue(objGetCommand.getData().get(i).getKey());
                            filterList.add(objCommonDialogResponse);
                            db.insertStatus(String.valueOf(objGetCommand.getData().get(i).getValue()), objGetCommand.getData().get(i).getKey());
                        }

                        dialog_search_list(edtStatus, filterList);
                    } else {
                        Utils.dialogForMessage(getActivity(), response.body().getMessage());
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());

            }

            @Override
            public void onFailure(Call<GetStatus> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });

    }

    void getGatway(String token) {
        dialog_wait.show();
        filterList.clear();
        objApi.getGatwayList(token, "", AppConstants.IS_SECURED).enqueue(new Callback<GetGatway>() {

            @Override
            public void onResponse(Call<GetGatway> call, Response<GetGatway> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        GetGatway objGetCommand = response.body();
                        db.deleteTableData(DBHelper.STATUS_TABLE_NAME);
                        filterList.clear();
                        for (int i = 0; i < objGetCommand.getData().size(); i++) {
                            CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                            //objCommonDialogResponse.setId(Integer.valueOf(objGetCommand.getData().get(i).getId()));
                            objCommonDialogResponse.setValue(String.valueOf(objGetCommand.getData().get(i).getGatewayName()));
                            filterList.add(objCommonDialogResponse);
                            db.insertGatway(objGetCommand.getData().get(i).getId(), objGetCommand.getData().get(i).getGatewayName());
                        }
                        dialog_search_list(edtGatwayName, filterList);
                    } else {
                        Utils.dialogForMessage(getActivity(), response.body().getMessage());
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<GetGatway> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    void getTrackId(String token, String trackid, final ListDialogSentCommandAdapter objAdapter) {
        //dialog_wait.show();
        filterList.clear();
        objApi.getTrackId(token, trackid, AppConstants.IS_SECURED).enqueue(new Callback<TrackIds>() {
            @Override
            public void onResponse(Call<TrackIds> call, Response<TrackIds> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        tvNoRecord.setVisibility(View.GONE);
                        objRecyclerViewSearchDialogTrackID.setVisibility(View.VISIBLE);

                        TrackIds objGetCommand = response.body();
                        filterList.clear();
                        for (int i = 0; i < objGetCommand.getData().size(); i++) {
                            CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                            objCommonDialogResponse.setValue(objGetCommand.getData().get(i));
                            objCommonDialogResponse.setId(i);
                            filterList.add(objCommonDialogResponse);
                        }
                        objAdapter.notifyDataSetChanged();
                        //dialog_search_list(edtTrackId, false, filterList);

                    } else {
                        objAdapter.notifyDataSetChanged();
                        tvNoRecord.setVisibility(View.VISIBLE);
                        objRecyclerViewSearchDialogTrackID.setVisibility(View.GONE);
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<TrackIds> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    ListDialogSentCommandAdapter objAdapter;
    ArrayList<CommonDialogResponse> mListFilter;
    ArrayList<CommonDialogResponse> mListAll;

    void dialog_search_list(final TextInputEditText editText, ArrayList<CommonDialogResponse> mList) {
        //filterList.clear();
        //filterList = mlistDialog;

        mListFilter = mList;
        mListAll = mList;

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView btGetDirection = dialog.findViewById(R.id.btGetDirection);
        btGetDirection.setVisibility(View.GONE);

        TextView tvNoRecord = dialog.findViewById(R.id.tvNoRecord);

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);

        if (mListFilter.size() == 0) {
            tvNoRecord.setVisibility(View.VISIBLE);
            objRecyclerView.setVisibility(View.GONE);
        } else {
            tvNoRecord.setVisibility(View.GONE);
            objRecyclerView.setVisibility(View.VISIBLE);
        }

        objAdapter = new ListDialogSentCommandAdapter(getActivity(), mListFilter,false, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String value = response.getValue().toString();
                editText.setText(value);
                SharedPreferences.Editor editor = spf.edit();

                if (editText == edtCommanddname) {
                    cmd_id = String.valueOf(response.getId());
                    editor.putString(AppConstants.SENT_C_CMD_NAME_VALUE, value);
                    editor.putString(AppConstants.SENT_C_CMD_NAME_ID, cmd_id);
                }
                if (editText == edtStatus) {
                    status_id = String.valueOf(response.getId());
                    editor.putString(AppConstants.SENT_C_STATUS_VALUE, value);
                    editor.putString(AppConstants.SENT_C_STATUS_ID, status_id);
                }

                if (editText == edtGatwayName) {
                    editor.putString(AppConstants.SENT_C_GATWAY_VALUE, value);
                }
                editor.apply();
                /*if (flag) {
                    cmd_id = String.valueOf(response.getId());
                }
                if(isStatus){
                    status_id=String.valueOf(response.getId());
                }*/

                dialog.dismiss();
            }
        });

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(objAdapter);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        objSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);


        objSearchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {

                        //ArrayList<CommonDialogResponse> temp = new ArrayList<>();
                        //temp.addAll(mListAll);

                        ArrayList<CommonDialogResponse> temp = new ArrayList<>();
                        ArrayList<CommonDialogResponse> temp2 = new ArrayList<>();
                        if (editText == edtStatus) {
                            temp.addAll(db.getAllStatus());
                        } else if (editText == edtGatwayName) {
                            temp.addAll(db.getAllGatway());
                        } else if (editText == edtCommanddname) {
                            temp.addAll(db.getAllCmd());
                        }
                        temp2 = objAdapter.filter(s, temp);

                        if (temp2.size() == 0) {
                            tvNoRecord.setVisibility(View.VISIBLE);
                            objRecyclerView.setVisibility(View.GONE);
                        } else {
                            tvNoRecord.setVisibility(View.GONE);
                            objRecyclerView.setVisibility(View.VISIBLE);
                        }

                        /*if (TextUtils.isEmpty(s)) {
                            //mListFilter.clear();
                            //mFiltered=mListAll;
                            objAdapter.notifyDataSetChanged();
                        } else {
                            //Log.i(AppConstants.TAG+"###", mList.size() + "");
                            //filter(s, temp);

                            //objAdapter.filter(s);
                            //objAdapter.getFilter();
                        }*/

                        return false;
                    }
                });

        /*if (editText == edtCommanddname) {
            //getCommands(token, objAdapter, objSearchView, editText);
        } else if (editText == edtStatus) {
            getStatus(token, objAdapter);
        } else if (editText == edtGatwayName) {
            getGatway(token, objAdapter);
        } else*/

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    public void filter(final String text, ArrayList<CommonDialogResponse> mList2) {
        // Searching could be complex..so we will dispatch it to a different thread...
        // Clear the filter list
        //mListTemp.clear();
        mListFilter.clear();
        // If there is no search value, then add all original list items to filter list
        // Iterate in the original List and add it to filter list...
        for (CommonDialogResponse item : mList2) {
            if (item.getValue().toLowerCase().contains(text.toLowerCase()) ||
                    item.getValue().toLowerCase().contains(text.toLowerCase())) {
                // Adding Matched items
                mListFilter.add(item);
            }
        }
        objAdapter.notifyDataSetChanged();
    }


    RecyclerView objRecyclerViewSearchDialogTrackID;

    void dialog_search_list_api_call(final TextInputEditText editText) {
        //filterList.clear();
        //filterList = mlistDialog;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView btGetDirection = dialog.findViewById(R.id.btGetDirection);
        btGetDirection.setVisibility(View.GONE);

        tvNoRecord = dialog.findViewById(R.id.tvNoRecord);

        objRecyclerViewSearchDialogTrackID = dialog.findViewById(R.id.rvDialog);
        objRecyclerViewSearchDialogTrackID.setHasFixedSize(true);
        objRecyclerViewSearchDialogTrackID.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerViewSearchDialogTrackID.setLayoutManager(linearLayoutManager);

        ArrayList<CommonDialogResponse> mList1 = new ArrayList<>();
        objAdapter = new ListDialogSentCommandAdapter(getActivity(), filterList,false, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String value = response.getValue().toString();
                editText.setText(value);

                SharedPreferences.Editor edit = spf.edit();
                if (editText == edtTrackId) {
                    edit.putString(AppConstants.SENT_C_TRACK_ID_VALUE, value);
                }
                edit.apply();
                dialog.dismiss();
            }
        });

        objRecyclerViewSearchDialogTrackID.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerViewSearchDialogTrackID.setAdapter(objAdapter);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        objSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        if (editText.getId() == R.id.edtTrackId) {
                            //dialog_search_list(edtTrackId, false, filterList);
                            getTrackId(token, s, objAdapter);
                        }
                        return false;
                    }
                });

        if (editText == edtTrackId) {
            getTrackId(token, "", objAdapter);
        }

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    void dummyData() {
        mList.clear();
        for (int i = 0; i < 10; i++) {
            com.cl.lg.pojo.ListResponse.List objList = new com.cl.lg.pojo.ListResponse.List();
            objList.setMacAddress("" + i);
            objList.setCreated("24/3/2019");
            objList.setSlcId("25511");
            mList.add(objList);
        }
        tvNorecordsList.setVisibility(View.GONE);
        rvSentCommand.setVisibility(View.VISIBLE);
        adapterSentCmd.notifyDataSetChanged();
    }

    void getData(int pg) {
        if (pg == 1) {
            mListFinal.clear();
            adapterSentCmd.notifyDataSetChanged();
            dialog_wait.show();
            progressBar.setVisibility(View.GONE);
        }
        else
            progressBar.setVisibility(View.VISIBLE);

        String jsonfinal = null;
        try {
            String jsonStr = "{SendCommandobj:[{" +
                    "\"SLCID\":\"\"," +
                    "\"Gateway\":\"" + edtGatwayName.getText().toString() + "\"," +
                    "\"CommandName\":\"" + cmd_id + "\"," +
                    "\"Status\":\"" + status_id + "\"," +
                    "\"SentBy\":\"\"," +
                    "\"TrackId\":\"" + edtTrackId.getText().toString() + "\"," +
                    "\"FromDate\":\"" + edtFromDate.getText().toString() + "\"," +
                    "\"ToDate\":\"" + edtToDate.getText().toString() + "\"" +
                    "}]}";

            JSONObject jsonObj = new JSONObject(jsonStr);
            jsonfinal = jsonObj.toString();
            Log.i(AppConstants.TAG, "jsonFinal: " + jsonfinal);

        } catch (Exception e) {
        }


        JsonParser jsonParser = new JsonParser();
        JsonObject gsonObject = (JsonObject) jsonParser.parse(jsonfinal);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonfinal);

        Log.d(AppConstants.TAG, "PAGE:" + pg);

        objApi.getSentCommand(token, String.valueOf(pg), gsonObject, AppConstants.IS_SECURED, "10").enqueue(new Callback<SentCommand>() {
            @Override
            public void onResponse(Call<SentCommand> call, Response<SentCommand> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                progressBar.setVisibility(View.GONE);
                if (response.code() == 200) {
                    SentCommand objSentCommand = response.body();
                    if (objSentCommand.getStatus().equalsIgnoreCase("1")) {
                        rvSentCommand.setVisibility(View.VISIBLE);
                        tvNorecordsList.setVisibility(View.GONE);

                        if (objSentCommand.getData().getTrackNetworkList().size() != 0) {
                            totalRecords = Integer.parseInt(objSentCommand.getData().getTotalRecords());
                        }
                        List<TrackNetworkList> mList = objSentCommand.getData().getTrackNetworkList();
                        int size = mList.size();
                        ArrayList<TrackNetworkList> tempList = new ArrayList<>();
                        for (int i = 0; i < size; i++) {
                            TrackNetworkList obj = new TrackNetworkList();
                            obj.setTrackID(mList.get(i).getTrackID());
                            obj.setSentBy(mList.get(i).getSentBy());
                            obj.setGatewayName(mList.get(i).getGatewayName());
                            obj.setCommandname(mList.get(i).getCommandname());
                            obj.setStatusDetail(mList.get(i).getStatusDetail());
                            obj.setDetail(mList.get(i).getDetail());
                            obj.setCreatedon(mList.get(i).getCreatedon());
                            obj.setClientCommType(mList.get(i).getClientCommType());
                            //tempList.add(obj);
                            //mListFinal.add(obj);
                            mListFinal.add(mList.get(i));
                        }
                        //mListFinal.addAll(tempList);
                        //rvSentCommand.getRecycledViewPool().clear();
                        adapterSentCmd.notifyDataSetChanged();
                    } else {
                        rvSentCommand.setVisibility(View.GONE);
                        tvNorecordsList.setVisibility(View.VISIBLE);

                        //if(mListFinal.size()==0)
                        Utils.dialogForMessage(getActivity(),objUtils.getStringResourceByName(getActivity(), response.body().getMsg().toString()));
                    }
                } else {
                  /*rvSentCommand.setVisibility(View.GONE);
                    tvNorecordsList.setVisibility(View.VISIBLE);*/

                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<SentCommand> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                objUtils.dismissProgressDialog(dialog_wait);

                /*rvSentCommand.setVisibility(View.GONE);
                tvNorecordsList.setVisibility(View.VISIBLE);*/
            }
        });
    }

    ArrayList<CommonDialogResponse> dummyData2() {
        filterList.clear();
        for (int i = 0; i < 5; i++) {
            com.cl.lg.pojo.CommonDialogResponse objList = new com.cl.lg.pojo.CommonDialogResponse();
            objList.setValue("Test" + i);

            filterList.add(objList);
        }
        return filterList;
    }

    @Override
    public void onClickForControl(int position, TrackNetworkList objList) {
        //objUtils.loadFragment(new DetailListFragment(), getActivity());

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment

                Bundle objBundle = new Bundle();
                objBundle.putString("From", "SentCommand");
                objBundle.putString("track_id", objList.getTrackID());
                objBundle.putString("gateway_name", objList.getGatewayName());
                objBundle.putString("clientCommType",objList.getClientCommType());

                /*objBundle.putString(AppConstants.SENT_C_TRACK_ID_VALUE,edtTrackId.getText().toString());
                objBundle.putString(AppConstants.SENT_C_GATWAY_VALUE,edtGatwayName.getText().toString());
                objBundle.putString(AppConstants.SENT_C_FROM_DATE,edtFromDate.getText().toString());
                objBundle.putString(AppConstants.SENT_C_TO_DATE_VALE,edtToDate.getText().toString());

                objBundle.putString(AppConstants.SENT_C_CMD_NAME_VALUE,edtCommanddname.getText().toString());
                objBundle.putString(AppConstants.SENT_C_STATUS_VALUE,edtStatus.getText().toString());
                objBundle.putString(AppConstants.SENT_C_CMD_NAME_ID,cmd_id);
                objBundle.putString(AppConstants.SENT_C_STATUS_ID,status_id);*/

                objBundle.putInt(AppConstants.IS_SEARCH_ON_SENT_CMD, llSentCommands.getVisibility());

                DetailListFragment fragment = new DetailListFragment();
                fragment.setArguments(objBundle);

                //fragmentTransaction.replace(R.id.frm1, fragment);
                //fragmentTransaction.commit(); // save the changes
                //fragmentTransaction.addToBackStack(null);

                fragmentTransaction.add(R.id.frm1, fragment);
                fragmentTransaction.hide(SentCommandFragment.this);
                fragmentTransaction.addToBackStack(SentCommandFragment.class.getName());
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.edtCommanddname:
                //getCommands(token);
                if (Utils.isInternetAvailable(getActivity())) {
                    getCommands(token);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;
            case R.id.edtGatwayName:
                if (Utils.isInternetAvailable(getActivity())) {
                    getGatway(token);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                break;

            case R.id.edtStatus:

                if (Utils.isInternetAvailable(getActivity())) {
                    getStatus(token);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }


                break;

            case R.id.edtTrackId:
                //getTrackId(token, "");
                if (Utils.isInternetAvailable(getActivity())) {
                    dialog_search_list_api_call(edtTrackId);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }

                //dialog_search_list(edtTrackId, false, mList2);
                break;

            case R.id.edtFromDate:
                myCalendarFrom = Calendar.getInstance();
                objDatePickerDialogFrom =
                        new DatePickerDialog(getActivity(), fromDate, myCalendarFrom
                                .get(Calendar.YEAR), myCalendarFrom.get(Calendar.MONTH),
                                myCalendarFrom.get(Calendar.DAY_OF_MONTH));
                objDatePickerDialogFrom.getDatePicker().setMaxDate(myCalendarFrom.getTimeInMillis());
                objDatePickerDialogFrom.show();
                break;
            case R.id.edtToDate:
                if (!edtFromDate.getText().toString().equalsIgnoreCase("")) {
                    myCalendarTo = Calendar.getInstance();
                    DatePickerDialog objDatePickerDialog =
                            new DatePickerDialog(getActivity(), toDate, myCalendarTo
                                    .get(Calendar.YEAR), myCalendarTo.get(Calendar.MONTH),
                                    myCalendarTo.get(Calendar.DAY_OF_MONTH));
                    objDatePickerDialog.getDatePicker().setMinDate(myCalendarFrom.getTimeInMillis());
                    objDatePickerDialog.getDatePicker().setMaxDate(myCalendarTo.getTimeInMillis());
                    objDatePickerDialog.show();
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.chhose_from_date));
                }
                break;
            case R.id.ivFilter:
                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "searchSentCommand";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

                if (llSentCommands.getVisibility() == View.GONE) {
                    llSentCommands.setVisibility(View.VISIBLE);
                    resetSearchFields();
                } else {
                    llSentCommands.setVisibility(View.GONE);
                }
                break;

            case R.id.btnClear:
                if (Utils.isInternetAvailable(getActivity())) {
                    llSentCommands.setVisibility(View.VISIBLE);
                    resetSearchFields();
                    getData(1);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;
            case R.id.btnSearch:
                boolean isApiCall = false;
                if (Utils.isInternetAvailable(getActivity())) {
                    if (!edtFromDate.getText().toString().equals("")) {
                        if (edtToDate.getText().toString().equals(""))
                            Utils.dialogForMessage(getActivity(), getResources().getString(R.string.chhose_to_date));
                        else
                            isApiCall = true;
                    } else
                        isApiCall = true;
                } else
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));

                if (isApiCall)
                    getData(1);
                break;
        }
    }

}

      /*  HashMap<String,String> inner=new HashMap<>();
        inner.put("SLCID","");
        inner.put("Gateway",edtGatwayName.getText().toString());
        inner.put("CommandName",edtCommanddname.getText().toString());
        inner.put("Status",edtStatus.getText().toString());
        inner.put("SentBy","");
        inner.put("TrackId",edtTrackId.getText().toString());
        inner.put("FromDate",edtFromDate.getText().toString());
        inner.put("ToDate",edtToDate.getText().toString());

        JSONArray ary=null;
        HashMap<String, JSONArray> out=null;
        try {
            JSONObject object=new JSONObject(inner);
            ary=new JSONArray(object);

            out=new HashMap<>();
            out.put("SendCommandobj",ary);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject object=new JSONObject(out);
        String outStr=object.toString();
        Log.d(AppConstants.TAG,outStr.toString());*/

/*  SendcommandBody objBody = getSearchValues(
            edtCommanddname.getText().toString(),
            edtStatus.getText().toString(),
            edtFromDate.getText().toString(),
            edtToDate.getText().toString(),
            edtGatwayName.getText().toString(),
            edtTrackId.getText().toString());*/

//ObjectMapper mapper = new ObjectMapper();
//String jsonString = mapper.writeValueAsString(object);