
package com.cl.lg.pojo.SLCStatus2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class List {

    @SerializedName("roW_NUMBER")
    @Expose
    private Double roWNUMBER;
    @SerializedName("gatewayId")
    @Expose
    private Double gatewayId;
    @SerializedName("nmsStatus")
    @Expose
    private Double nmsStatus;
    @SerializedName("slcNo")
    @Expose
    private Integer slcNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("datetime")
    @Expose
    private String dateTime;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("d1@Lamp Status")
    @Expose
    private String d1LampStatus;
    @SerializedName("d3@Voltage Under Over")
    @Expose
    private String d3VoltageUnderOver;
    @SerializedName("d4@Lamp")
    @Expose
    private String d4Lamp;
    @SerializedName("d8@Communication")
    @Expose
    private String d8Communication;
    @SerializedName("d9@Driver")
    @Expose
    private String d9Driver;
    @SerializedName("d15@Tilt")
    @Expose
    private String d15Tilt;
    @SerializedName("a1@Voltage")
    @Expose
    private Double a1Voltage;
    @SerializedName("a2@Current")
    @Expose
    private Double a2Current;
    @SerializedName("a3@Watts")
    @Expose
    private Double a3Watts;
    @SerializedName("a4@Cumulative KiloWatt Hrs")
    @Expose
    private Double a4CumulativeKiloWattHrs;
    @SerializedName("a5@Burn Hrs")
    @Expose
    private Double a5BurnHrs;
    @SerializedName("a6@Dimming")
    @Expose
    private Double a6Dimming;
    @SerializedName("a7@Power Factor")
    @Expose
    private Double a7PowerFactor;
    @SerializedName("a9@Mode")
    @Expose
    private Double a9Mode;
    @SerializedName("tag8")
    @Expose
    private String tag8;

    boolean isChecked;

    String jsonString;

    public Double getRoWNUMBER() {
        return roWNUMBER;
    }

    public void setRoWNUMBER(Double roWNUMBER) {
        this.roWNUMBER = roWNUMBER;
    }

    public Double getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Double gatewayId) {
        this.gatewayId = gatewayId;
    }

    public Double getNmsStatus() {
        return nmsStatus;
    }

    public void setNmsStatus(Double nmsStatus) {
        this.nmsStatus = nmsStatus;
    }

    public Integer getSlcNo() {
        return slcNo;
    }

    public void setSlcNo(Integer slcNo) {
        this.slcNo = slcNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getD1LampStatus() {
        return d1LampStatus;
    }

    public void setD1LampStatus(String d1LampStatus) {
        this.d1LampStatus = d1LampStatus;
    }

    public String getD3VoltageUnderOver() {
        return d3VoltageUnderOver;
    }

    public void setD3VoltageUnderOver(String d3VoltageUnderOver) {
        this.d3VoltageUnderOver = d3VoltageUnderOver;
    }

    public String getD4Lamp() {
        return d4Lamp;
    }

    public void setD4Lamp(String d4Lamp) {
        this.d4Lamp = d4Lamp;
    }

    public String getD8Communication() {
        return d8Communication;
    }

    public void setD8Communication(String d8Communication) {
        this.d8Communication = d8Communication;
    }

    public String getD9Driver() {
        return d9Driver;
    }

    public void setD9Driver(String d9Driver) {
        this.d9Driver = d9Driver;
    }

    public String getD15Tilt() {
        return d15Tilt;
    }

    public void setD15Tilt(String d15Tilt) {
        this.d15Tilt = d15Tilt;
    }

    public Double getA1Voltage() {
        return a1Voltage;
    }

    public void setA1Voltage(Double a1Voltage) {
        this.a1Voltage = a1Voltage;
    }

    public Double getA2Current() {
        return a2Current;
    }

    public void setA2Current(Double a2Current) {
        this.a2Current = a2Current;
    }

    public Double getA3Watts() {
        return a3Watts;
    }

    public void setA3Watts(Double a3Watts) {
        this.a3Watts = a3Watts;
    }

    public Double getA4CumulativeKiloWattHrs() {
        return a4CumulativeKiloWattHrs;
    }

    public void setA4CumulativeKiloWattHrs(Double a4CumulativeKiloWattHrs) {
        this.a4CumulativeKiloWattHrs = a4CumulativeKiloWattHrs;
    }

    public Double getA5BurnHrs() {
        return a5BurnHrs;
    }

    public void setA5BurnHrs(Double a5BurnHrs) {
        this.a5BurnHrs = a5BurnHrs;
    }

    public Double getA6Dimming() {
        return a6Dimming;
    }

    public void setA6Dimming(Double a6Dimming) {
        this.a6Dimming = a6Dimming;
    }

    public Double getA7PowerFactor() {
        return a7PowerFactor;
    }

    public void setA7PowerFactor(Double a7PowerFactor) {
        this.a7PowerFactor = a7PowerFactor;
    }

    public Double getA9Mode() {
        return a9Mode;
    }

    public void setA9Mode(Double a9Mode) {
        this.a9Mode = a9Mode;
    }

    public String getTag8() {
        return tag8;
    }

    public void setTag8(String tag8) {
        this.tag8 = tag8;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getJsonString() {
        return jsonString;
    }

    public void setJsonString(String jsonString) {
        this.jsonString = jsonString;
    }
}
