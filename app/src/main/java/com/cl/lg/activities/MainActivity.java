package com.cl.lg.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cl.lg.R;
import com.cl.lg.fragment.HistoryDayburnerCountFragment;
import com.cl.lg.fragment.MapFragment;
import com.cl.lg.fragment.MoreFragment;
import com.cl.lg.fragment.ProfileFragment;
import com.cl.lg.fragment.SLCCommandsFragment;
import com.cl.lg.fragment.DashboardFragment;
import com.cl.lg.fragment.SentCommandFragment;
import com.cl.lg.fragment.StatusFragment;
import com.cl.lg.fragment.StatusFragment2;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.LanguageHelper;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.llDashboard)
    LinearLayout llDashboard;
    @BindView(R.id.imgDashboard)
    ImageView imgDashboard;
    @BindView(R.id.tvDashboard)
    TextView tvDashboard;

    @BindView(R.id.llCommands)
    LinearLayout llCommands;
    @BindView(R.id.imgCommands)
    ImageView imgCommands;
    @BindView(R.id.tvCommand)
    TextView tvCommand;

    @BindView(R.id.llStatus)
    LinearLayout llStatus;
    @BindView(R.id.imgStatus)
    ImageView imgStatus;
    @BindView(R.id.tvStatus)
    TextView tvStatus;

    @BindView(R.id.llMap)
    LinearLayout llMap;
    @BindView(R.id.imgMap)
    ImageView imgMap;
    @BindView(R.id.tvMap)
    TextView tvMap;

    @BindView(R.id.llSentCommands)
    LinearLayout llSentCommands;
    @BindView(R.id.imgSentCommands)
    ImageView imgSentCommands;
    @BindView(R.id.tvSentCommands)
    TextView tvSentCommands;

    @BindView(R.id.llProfile)
    LinearLayout llProfile;
    @BindView(R.id.imgProfile)
    ImageView imgProfile;
    @BindView(R.id.tvProfile)
    TextView tvProfile;

    @BindView(R.id.llMenu)
    LinearLayout llMenu;
    @BindView(R.id.imgMenu)
    ImageView imgMenu;
    @BindView(R.id.tvMenu)
    TextView tvMenu;

    @BindView(R.id.ivBgMain)
    ImageView ivBgMain;
    SharedPreferences spf;
    Utils objUtils;
    FirebaseAnalytics mFirebaseAnalytics;

    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    FragmentManager manager;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {

        ButterKnife.bind(MainActivity.this);

        Glide.with(this).load(R.drawable.bg_new).into(ivBgMain);
        manager = getFragmentManager();
        spf = getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        objUtils = new Utils();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        selectDashboard(true);

        objUtils.loadFragmentMain(new DashboardFragment(), MainActivity.this);
      /*  ft = manager.beginTransaction();
        ft.replace(R.id.frm1, new DashboardFragment(), "dashboard");
        ft.addToBackStack(null);
        ft.commit();*/

        llDashboard.setOnClickListener(this);
        llStatus.setOnClickListener(this);
        llSentCommands.setOnClickListener(this);
        llCommands.setOnClickListener(this);
        llMap.setOnClickListener(this);
        llProfile.setOnClickListener(this);
        llMenu.setOnClickListener(this);

        boolean isLangSelected = spf.getBoolean(AppConstants.LANGUAGE_SELECTED, false);
        String langCode = spf.getString(AppConstants.LANGUAGE_LOCALE, AppConstants.LANGUAGE_CODE_ENGLISH);

        if (!isLangSelected) {
            new Utils().switchLanguage(MainActivity.this, langCode, true);
            Log.i(AppConstants.TAG, "switchLang called");
        }

        //mFirebaseAnalytics.setCurrentScreen(MainActivity.this, "Main UI", null /* class override */);
    }

    public void selectDashboard(boolean mValue) {
        if (mValue) {
            //imgDashboard.setImageDrawable(getResources().getDrawable(R.drawable.dashboard_blue));
            imgDashboard.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorSkyBlueDashboard));
            tvDashboard.setTextColor(getResources().getColor(R.color.colorSkyBlueDashboard));
        } else {
            //imgDashboard.setImageDrawable(getResources().getDrawable(R.drawable.dashboard));
            tvDashboard.setTextColor(getResources().getColor(R.color.colorWhite));
            imgDashboard.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        }
    }

    public void selectStatus(boolean mValue) {
        if (mValue) {
            //imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.status_blue));
            imgStatus.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorSkyBlueDashboard));
            tvStatus.setTextColor(getResources().getColor(R.color.colorSkyBlueDashboard));
        } else {
            //imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.status));
            tvStatus.setTextColor(getResources().getColor(R.color.colorWhite));
            imgStatus.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        }
    }

    public void selectCommand(boolean mValue) {
        if (mValue)
            llCommands.setBackgroundColor(getResources().getColor(R.color.colorOffWhite));
        else
            llCommands.setBackgroundColor(getResources().getColor(R.color.colorGray));
    }

    public void selectSentCommand(boolean mValue) {
        if (mValue)
            llSentCommands.setBackgroundColor(getResources().getColor(R.color.colorOffWhite));
        else
            llSentCommands.setBackgroundColor(getResources().getColor(R.color.colorGray));
    }

    public void selectMap(boolean mValue) {
        if (mValue) {
            //imgMap.setImageDrawable(getResources().getDrawable(R.drawable.map_blue));
            imgMap.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorSkyBlueDashboard));
            tvMap.setTextColor(getResources().getColor(R.color.colorSkyBlueDashboard));
        } else {
            //imgMap.setImageDrawable(getResources().getDrawable(R.drawable.map));
            tvMap.setTextColor(getResources().getColor(R.color.colorWhite));
            imgMap.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        }
    }

    public void selectProfile(boolean mValue) {
        if (mValue) {
            //imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.profile_blue));
            imgProfile.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorSkyBlueDashboard));
        }
        else {
            //imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.profile));
            imgProfile.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        }
    }

    public void selectMenu(boolean mValue) {
        if (mValue) {
            //imgMenu.setImageDrawable(getResources().getDrawable(R.drawable.menu_blue));
            tvMenu.setTextColor(getResources().getColor(R.color.colorSkyBlueDashboard));
            imgMenu.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorSkyBlueDashboard));
        } else {
            //imgMenu.setImageDrawable(getResources().getDrawable(R.drawable.menu));
            tvMenu.setTextColor(getResources().getColor(R.color.colorWhite));
            imgMenu.setColorFilter(ContextCompat.getColor(MainActivity.this, R.color.colorWhite));
        }
    }


    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getFragmentManager();

        HistoryDayburnerCountFragment dFragment = (HistoryDayburnerCountFragment) fragmentManager.findFragmentByTag("HISTORY");
        FragmentTransaction ft = fragmentManager.beginTransaction();


        if (dFragment != null) {
            dFragment.reset();
            ft.detach(dFragment);
            //fragmentManager.popBackStackImmediate();
            //return;
        }

        if (fragmentManager.getBackStackEntryCount() == 1) {
            //We have fragments on the backstack that are poppable
            fragmentManager.popBackStackImmediate();
            //fragmentManager.popBackStack();
            Log.i("--*", "else if");
        } else {
            super.onBackPressed();
            Log.i("--*", "else");
        }


    }

    private Fragment getCurrentFragment() {
        androidx.fragment.app.FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        int stackCount = fragmentManager.getBackStackEntryCount();
        if (fragmentManager.getFragments() != null)
            return fragmentManager.getFragments().get(stackCount > 0 ? stackCount - 1 : stackCount);
        else return null;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.llDashboard:
                selectDashboard(true);
                selectCommand(false);
                selectStatus(false);
                selectSentCommand(false);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);

                //spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                objUtils.loadFragment(new DashboardFragment(), MainActivity.this);

              /*  ft = manager.beginTransaction();
                ft.replace(R.id.frm1, new DashboardFragment(), "dashboard");
                ft.addToBackStack(null);
                ft.commit();*/
                break;

            case R.id.llCommands:
                selectDashboard(false);
                selectCommand(true);
                selectStatus(false);
                selectSentCommand(false);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);

                objUtils.loadFragment(new SLCCommandsFragment(), MainActivity.this);
                break;

            case R.id.llStatus:
                selectDashboard(false);
                selectCommand(false);
                selectStatus(true);
                selectSentCommand(false);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);
                //spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                //objUtils.loadFragment(new StatusFragment2(), MainActivity.this);
                //objUtils.loadFragment(new StatusFragment(), MainActivity.this);

                objUtils.openStatus(MainActivity.this,
                        spf.getInt(AppConstants.UI_ID, R.id.ll0),
                        spf.getInt(AppConstants.IS_SEARCH_ON, View.GONE),
                        spf.getBoolean(AppConstants.IS_NEAR_ME, false),
                        View.GONE);

               /* FragmentManager fm = MainActivity.this.getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                StatusFragment fragment = new StatusFragment();

                Bundle objBundle = new Bundle();
                fragment.setArguments(objBundle);
*/
                //fragmentTransaction.replace(R.id.frm1, fragment);
                //fragmentTransaction.addToBackStack(null);


                /*Fragment current=getCurrentFragment();
                fragmentTransaction.add(R.id.frm1, fragment);
                fragmentTransaction.hide();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
*/

                /*ft = manager.beginTransaction();
                DashboardFragment dFragment = (DashboardFragment) manager.findFragmentByTag("dashboard");
                ft.add(R.id.frm1, new StatusFragment(), "status");
                if (dFragment != null) {
                    ft.hide(new DashboardFragment());
                    ft.addToBackStack(DashboardFragment.class.getName());
                }
                ft.commit();*/

                //objUtils.loadFragment(new StatusFragment(), MainActivity.this);
                break;

            case R.id.llSentCommands:
                selectDashboard(false);
                selectCommand(false);
                selectStatus(false);
                selectSentCommand(true);
                selectMap(false);
                selectProfile(false);
                selectMenu(false);
                objUtils.loadFragment(new SentCommandFragment(), MainActivity.this);
                break;

            case R.id.llMap:
                selectDashboard(false);
                selectStatus(false);
                selectMap(true);
                selectProfile(false);
                selectMenu(false);
                spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                objUtils.loadFragment(new MapFragment(), MainActivity.this);

                break;

            case R.id.llProfile:
                selectDashboard(false);
                selectStatus(false);
                selectMap(false);
                selectProfile(true);
                selectMenu(false);
                objUtils.loadFragment(new ProfileFragment(), MainActivity.this);
                break;

            case R.id.llMenu:
                selectDashboard(false);
                selectStatus(false);
                selectMap(false);
                selectMenu(true);
                spf.edit().putInt(AppConstants.IS_SEARCH_ON_DETAIL, View.GONE).apply();
                // objUtils.loadFragment(new ProfileFragment(), MainActivity.this);
                objUtils.loadFragment(new MoreFragment(), MainActivity.this);
                break;
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        spf = base.getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        super.attachBaseContext(LanguageHelper.wrap(base, Utils.getDeviceLocale(spf)));
    }
}

 /* new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.back_press_title_dialog))
                .setMessage(getResources().getString(R.string.back_press_msg_dialog))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no), null)
                .show();*/

        /*if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }*/

//Fragment homeFrag = fragmentManager.findFragmentByTag("0");
//int fragments = fragmentManager.getBackStackEntryCount();

   /*     if (fragments == 1) {
            finish();
            Log.i("--*","finish");
        } else*/