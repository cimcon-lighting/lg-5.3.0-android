package com.cl.lg;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.cl.lg.networking.API;
import com.cl.lg.utils.AppConstants;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LG extends Application {

    SharedPreferences spf;
    String apiURL = "";

    public LG() {

    }

    public API networkCall(Context context, boolean isFromSecurtyCodeUI){
        Retrofit retrofit;
        spf = context.getSharedPreferences(AppConstants.SPF, MODE_PRIVATE);
        if (isFromSecurtyCodeUI)
            apiURL = AppConstants.BASE_URL;
        else
            apiURL = spf.getString(AppConstants.API_URL, "");

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if (AppConstants.isLogDisplay) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.connectTimeout(180, TimeUnit.SECONDS);
        httpClient.readTimeout(180, TimeUnit.SECONDS);
        httpClient.addInterceptor(interceptor);

        Retrofit.Builder builder =
                new Retrofit.Builder()
                        .baseUrl(apiURL)
                        .addConverterFactory(GsonConverterFactory.create());

        retrofit = builder.client(httpClient.build())
                .build();
        return retrofit.create(API.class);
    }

}