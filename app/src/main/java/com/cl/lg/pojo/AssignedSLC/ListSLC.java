
package com.cl.lg.pojo.AssignedSLC;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListSLC {

    @SerializedName("gatewayId")
    @Expose
    private String gatewayId;
    @SerializedName("slcNo")
    @Expose
    private String slcNo;
    @SerializedName("slcName")
    @Expose
    private String slcName;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusName")
    @Expose
    private String statusName;
    @SerializedName("macAddress")
    @Expose
    private String macAddress;
    @SerializedName("lastCommDate")
    @Expose
    private String lastCommDate;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @SerializedName("uid")
    @Expose
    private String uid

            ;

    public String getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(String gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getSlcNo() {
        return slcNo;
    }

    public void setSlcNo(String slcNo) {
        this.slcNo = slcNo;
    }

    public String getSlcName() {
        return slcName;
    }

    public void setSlcName(String slcName) {
        this.slcName = slcName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getLastCommDate() {
        return lastCommDate;
    }

    public void setLastCommDate(String lastCommDate) {
        this.lastCommDate = lastCommDate;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
