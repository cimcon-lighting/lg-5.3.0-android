
package com.cl.lg.pojo.FaultySLCMaster;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlcList {

    @SerializedName("dataType")
    @Expose
    private Integer dataType;
    @SerializedName("slcNo")
    @Expose
    private String slcNo;
    @SerializedName("slcName")
    @Expose
    private String slcName;
    @SerializedName("deviceAddress")
    @Expose
    private String deviceAddress;
    @SerializedName("macAddress")
    @Expose
    private String macAddress;
    @SerializedName("firstOccurrence")
    @Expose
    private String firstOccurrence;
    @SerializedName("dateTimeField")
    @Expose
    private String dateTimeField;
    @SerializedName("rtuGroupName")
    @Expose
    private String rtuGroupName;
    @SerializedName("fault")
    @Expose
    private String fault;
    @SerializedName("totalrows")
    @Expose
    private Integer totalrows;

    String latitude;
    String longitude;

    int tvId;

    public int getTvId() {
        return tvId;
    }

    public void setTvId(int tvId) {
        this.tvId = tvId;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getSlcNo() {
        return slcNo;
    }

    public void setSlcNo(String slcNo) {
        this.slcNo = slcNo;
    }

    public String getSlcName() {
        return slcName;
    }

    public void setSlcName(String slcName) {
        this.slcName = slcName;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public void setDeviceAddress(String deviceAddress) {
        this.deviceAddress = deviceAddress;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getFirstOccurrence() {
        return firstOccurrence;
    }

    public void setFirstOccurrence(String firstOccurrence) {
        this.firstOccurrence = firstOccurrence;
    }

    public String getDateTimeField() {
        return dateTimeField;
    }

    public void setDateTimeField(String dateTimeField) {
        this.dateTimeField = dateTimeField;
    }

    public String getRtuGroupName() {
        return rtuGroupName;
    }

    public void setRtuGroupName(String rtuGroupName) {
        this.rtuGroupName = rtuGroupName;
    }

    public String getFault() {
        return fault;
    }

    public void setFault(String fault) {
        this.fault = fault;
    }

    public Integer getTotalrows() {
        return totalrows;
    }

    public void setTotalrows(Integer totalrows) {
        this.totalrows = totalrows;
    }

}
