package com.cl.lg.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.cl.lg.R;
import com.cl.lg.pojo.DialogList;
import com.cl.lg.utils.AppConstants;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDialogAdapter extends RecyclerView.Adapter<ListDialogAdapter.MyViewHolderList> {

    Context mContext;
    List<DialogList> mList;
    private MyCallbackForControlDialog objMyCallbackForControl;

    public interface MyCallbackForControlDialog {
        void onClickForControl(int position, String slcNo);
    }

    public ListDialogAdapter(Context mContext, List<DialogList> mList, MyCallbackForControlDialog objMyCallbackForControl) {
        this.mList = mList;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_dialog_list, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {

        //myViewHolder.tvAssetKey.setText(mList.get(i).getName());
        if(i%2==0) {
            myViewHolder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorLightGray));
        }else{
            myViewHolder.llTitles.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        }
        myViewHolder.llTitles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickForControl(i, mList.get(i).getName());
                Log.i(AppConstants.TAG, "List: " + mList.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llTitles)
        LinearLayout llTitles;

        //@BindView(R.id.tvslcName)
        //TextView tvAssetKey;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}