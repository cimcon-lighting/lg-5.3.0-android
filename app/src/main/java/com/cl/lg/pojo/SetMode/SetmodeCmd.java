
package com.cl.lg.pojo.SetMode;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetmodeCmd {

    @SerializedName("data")
    @Expose
    private List<DatumSetMode> data = null;
    @SerializedName("status")
    @Expose
    private String status;

    public List<DatumSetMode> getData() {
        return data;
    }

    public void setData(List<DatumSetMode> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
