package com.cl.lg.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusListAdapter extends RecyclerView.Adapter<StatusListAdapter.MyViewHolderList> {

    Context mContext;
    List<com.cl.lg.pojo.SLCStatus2.List> mList;


    private MyCallbackForControl objMyCallbackForControl;
    boolean isInstaller = false;
    boolean isFromDetails = false;
    boolean isFromNever = false;

    private int selectedPosition = -1;// no selection by default

    public interface MyCallbackForControl {
        void onClickStatusUI(int position, com.cl.lg.pojo.SLCStatus2.List objListDetail);
    }

    public StatusListAdapter(Context mContext, List<com.cl.lg.pojo.SLCStatus2.List> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl, boolean isFromDetails, boolean isFromNever) {
        mList = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        isInstaller = false;
        this.isFromDetails = isFromDetails;
        this.isFromNever = isFromNever;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_status4, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderList myViewHolder, final int i) {

        final com.cl.lg.pojo.SLCStatus2.List objList = mList.get(i);

        myViewHolder.tvSlcId.setText(String.valueOf(objList.getSlcNo()));
        myViewHolder.tvslcName.setText(objList.getName());

        String date = objList.getDateTime();
        if (!objList.getDateTime().toString().equalsIgnoreCase("")) {
            if (date.contains("T")) {
                String[] ary = date.split("T");
                String finalStr = ary[0] + " " + ary[1];
                myViewHolder.tvLastUpdate.setText(finalStr);
                myViewHolder.ivArrow.setVisibility(View.VISIBLE);

                myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        objMyCallbackForControl.onClickStatusUI(i, mList.get(i));
                        //Log.i(AppConstants.TAG, "SLCID: " + mList.get(i).getId());
                    }
                });

            } else {
                myViewHolder.ivArrow.setVisibility(View.GONE);
            }

        } else {
            myViewHolder.tvLastUpdate.setText("N/A");
            myViewHolder.ivArrow.setVisibility(View.GONE);
        }



        if (isFromDetails) {
            myViewHolder.cbStatus.setVisibility(View.GONE);
            myViewHolder.llstatusLine2.setVisibility(View.GONE);
            if (isFromNever)
                myViewHolder.ivTempArrow.setVisibility(View.GONE);
            else
                myViewHolder.ivTempArrow.setVisibility(View.VISIBLE);
        } else {
            myViewHolder.cbStatus.setVisibility(View.VISIBLE);
            myViewHolder.llstatusLine2.setVisibility(View.VISIBLE);
            myViewHolder.ivTempArrow.setVisibility(View.GONE);
        }

        if (i % 2 == 0) {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));
        }

        if (objList.getD1LampStatus().equals("0"))
            myViewHolder.ivLS.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));
        else
            myViewHolder.ivLS.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));

        if (objList.getD9Driver().equals("0"))
            myViewHolder.ivD.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));
        else
            myViewHolder.ivD.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));

        myViewHolder.tvBhs.setText(String.valueOf(objList.getA5BurnHrs()));
        myViewHolder.tvVolt.setText(String.valueOf(objList.getA1Voltage()));
        myViewHolder.tvCur.setText(String.valueOf(objList.getA2Current()));

        //in some cases, it will prevent unwanted situations
        myViewHolder.cbStatus.setOnCheckedChangeListener(null);
        myViewHolder.cbStatus.setChecked(mList.get(i).isChecked());

        myViewHolder.cbStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mList.get(i).setChecked(isChecked);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvSlcId)
        TextView tvSlcId;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvLastUpdate)
        TextView tvLastUpdate;

        @BindView(R.id.cbStatus)
        CheckBox cbStatus;

        /*@BindView(R.id.ivLC)
        ImageView ivLC;*/


        @BindView(R.id.tvBhs)
        TextView tvBhs;

        @BindView(R.id.tvVolt)
        TextView tvVolt;

        @BindView(R.id.ivTempArrow)
        ImageView ivTempArrow;

        @BindView(R.id.llstatusLine2)
        LinearLayout llstatusLine2;

        @BindView(R.id.tvCur)
        TextView tvCur;

        @BindView(R.id.ivLS)
        ImageView ivLS;
        @BindView(R.id.ivD)
        ImageView ivD;

        @BindView(R.id.ivArrow)
        ImageView ivArrow;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

  /*  if (objList.getDateTime().equalsIgnoreCase("")) {
            myViewHolder.tvLastUpdate.setText("NA");
            myViewHolder.ivArrow.setVisibility(View.GONE);

        }else
            myViewHolder.ivArrow.setVisibility(View.VISIBLE);
*/
