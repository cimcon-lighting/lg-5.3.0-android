
package com.cl.lg.pojo.Map;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.sharewire.googlemapsclustering.ClusterItem;

public class Datum implements ClusterItem {

    @SerializedName("a")
    @Expose
    private String a;
    @SerializedName("b")
    @Expose
    private String b;
    @SerializedName("c")
    @Expose
    private Double c;
    @SerializedName("d")
    @Expose
    private Double d;
    @SerializedName("e")
    @Expose
    private String e;
    @SerializedName("f")
    @Expose
    private String f;
    @SerializedName("g")
    @Expose
    private String g;

    private String snippet;
    private String title;
    private LatLng mPosition;

    private String tag;

    public Datum(Double lat, Double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public Double getC() {
        return c;
    }

    public void setC(Double c) {
        this.c = c;
    }

    public Double getD() {
        return d;
    }

    public void setD(Double d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }

    public void setF(String f) {
        this.f = f;
    }

    public String getG() {
        return g;
    }

    public void setG(String g) {
        this.g = g;
    }

    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public double getLatitude() {
        return mPosition.latitude;
    }

    @Override
    public double getLongitude() {
        return mPosition.longitude;
    }

    @Nullable
    @Override
    public String getTitle() {
        return title.toString();
    }

    @Nullable
    @Override
    public String getSnippet() {
        return snippet;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
