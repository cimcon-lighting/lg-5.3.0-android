
package com.cl.lg.pojo.ListResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("List")
    @Expose
    private List<com.cl.lg.pojo.ListResponse.List> list;
    @SerializedName("tot_no_of_records")
    @Expose
    private String totNoOfRecords;

    public List<com.cl.lg.pojo.ListResponse.List> getList() {
        return list;
    }

    public void setList(List<com.cl.lg.pojo.ListResponse.List> list) {
        this.list = list;
    }

    public String getTotNoOfRecords() {
        return totNoOfRecords;
    }

    public void setTotNoOfRecords(String totNoOfRecords) {
        this.totNoOfRecords = totNoOfRecords;
    }

}
