package com.cl.lg.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.SentCommand.TrackNetworkList;
import com.cl.lg.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SentCommandAdapter extends RecyclerView.Adapter<SentCommandAdapter.MyViewHolderList> {

    Context mContext;
    List<TrackNetworkList> listRecentlyInstalleds;

    private MyCallbackForControl objMyCallbackForControl;
    boolean isInstaller = false;

    public interface MyCallbackForControl {
        void onClickForControl(int position, TrackNetworkList objList);
    }

    public SentCommandAdapter(Context mContext, List<TrackNetworkList> listRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl) {
        this.listRecentlyInstalleds = listRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        isInstaller = false;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_sent_command, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {
        if (i % 2 == 0)
            myViewHolder.llSentCommnad.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));
        else
            myViewHolder.llSentCommnad.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));

        TrackNetworkList objRecentlyInstalled = listRecentlyInstalleds.get(i);

        myViewHolder.tvTrackId.setText(objRecentlyInstalled.getTrackID());
        myViewHolder.tvCmdName.setText(objRecentlyInstalled.getCommandname());
        myViewHolder.tvSent.setText(objRecentlyInstalled.getSentBy());

        String test = String.valueOf(objRecentlyInstalled.getStatusDetail());
        myViewHolder.tvStatus.setText(test);
        myViewHolder.tvTime.setText(objRecentlyInstalled.getCreatedon());
        myViewHolder.tvDetail.setText(objRecentlyInstalled.getDetail());

        final int position = i;
        myViewHolder.llSentCommnad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              if(!listRecentlyInstalleds.get(position).getSendstatus().equalsIgnoreCase("3")) {
                    objMyCallbackForControl.onClickForControl(position, listRecentlyInstalleds.get(position));
                    Log.i(AppConstants.TAG, "SLCID: " + listRecentlyInstalleds.get(position).getTrackID());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listRecentlyInstalleds.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llSentCommnad)
        LinearLayout llSentCommnad;

        //@BindView(R.id.llList)
        //LinearLayout llList;

        @BindView(R.id.tvTrackId)
        TextView tvTrackId;

        @BindView(R.id.tvCmdName)
        TextView tvCmdName;

        @BindView(R.id.tvSent)
        TextView tvSent;

        @BindView(R.id.tvStatus)

        TextView tvStatus;

        @BindView(R.id.tvTime)
        TextView tvTime;

        @BindView(R.id.tvDetail)
        TextView tvDetail;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}