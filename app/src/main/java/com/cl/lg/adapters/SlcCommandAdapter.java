package com.cl.lg.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.utils.AppConstants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SlcCommandAdapter extends RecyclerView.Adapter<SlcCommandAdapter.MyViewHolderList> {

    Context mContext;

    List<com.cl.lg.pojo.ListResponse.List> listRecentlyInstalleds;

    private MyCallbackForControl objMyCallbackForControl;
    boolean isInstaller = false;

    public interface MyCallbackForControl {
        void onClickForControl(int position, String slcNo);
    }

    public SlcCommandAdapter(Context mContext, List<com.cl.lg.pojo.ListResponse.List> objRecentlyInstalleds, MyCallbackForControl objMyCallbackForControl) {
        listRecentlyInstalleds = objRecentlyInstalleds;
        this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
        isInstaller = false;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_slc_command2, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {

        if (i % 2 == 0)
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorSkyBlueList));
        else
            myViewHolder.llList.setBackgroundColor(mContext.getResources().getColor(R.color.colorWhite));

        com.cl.lg.pojo.ListResponse.List objRecentlyInstalled = listRecentlyInstalleds.get(i);

        myViewHolder.tvSlcId.setText(objRecentlyInstalled.getSlcId());
        myViewHolder.tvslcName.setText("test slc");
        myViewHolder.tvMacAddress.setText("123456");
        myViewHolder.tvLampType.setText("Lamp Type "+i );

        //myViewHolder.tvDetail.setText(objRecentlyInstalled.getCreated());

        myViewHolder.llList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objMyCallbackForControl.onClickForControl(i, listRecentlyInstalleds.get(i).getId());
                Log.i(AppConstants.TAG, "SLCID: " + listRecentlyInstalleds.get(i).getId());
            }
        });
        String syncStatus = objRecentlyInstalled.getSync();
    }

    @Override
    public int getItemCount() {
        return listRecentlyInstalleds.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {



        @BindView(R.id.llList)
        LinearLayout llList;

        @BindView(R.id.tvSlcId)
        TextView tvSlcId;

        @BindView(R.id.tvslcName)
        TextView tvslcName;

        @BindView(R.id.tvMacAddress)
        TextView tvMacAddress;

        @BindView(R.id.tvLampType)
        TextView tvLampType;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}