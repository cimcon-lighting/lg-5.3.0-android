package com.cl.lg.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;

import com.cl.lg.activities.MainActivity;
import com.google.android.material.textfield.TextInputEditText;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.adapters.GatwayDetailsAdapter;
import com.cl.lg.adapters.ListDialogSentCommandAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.GatewayDetails.GatewayMaster;
import com.cl.lg.pojo.GatewayDetails.List;
import com.cl.lg.pojo.GetGatwaylist.GetGatway;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.DBHelper;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GatewayInventory extends Fragment implements View.OnClickListener, GatwayDetailsAdapter.MyCallbackForControl {

    @BindView(R.id.llBackProfile)
    LinearLayout llBackProfile;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.llSearchGateway)
    LinearLayout llSearchGateway;

    @BindView(R.id.edtGatwayID)
    TextInputEditText edtGatwayID;

    @BindView(R.id.edtGatwayName)
    TextInputEditText edtGatwayName;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    @BindView(R.id.btnClear)
    Button btnClear;

    @BindView(R.id.rvGatway)
    RecyclerView rvGatway;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout objSwipeRefreshLayout;

    @BindView(R.id.progressBarGatway)
    ProgressBar progressBarGatway;

    Utils objUtils;

    View view;

    API objApi;
    String token;

    ProgressDialog dialog_wait;
    SharedPreferences spf;
    String type = "";
    Bundle objBundle;

    GatwayDetailsAdapter adapter;
    EndlessRecyclerViewScrollListener scrollListener;
    int totalcount;

    ArrayList<List> mListFinal2;

    final ArrayList<CommonDialogResponse> filterList = new ArrayList<>();
    DBHelper db;

    private FirebaseAnalytics mFirebaseAnalytics;
    int SearchVisibillity = View.GONE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gatway_inventory, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "gatewayList", null /* class override */);

        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        db = new DBHelper(getActivity());

        if (getArguments() != null) {
            objBundle = getArguments();
            type = objBundle.getString(AppConstants.GATWAY_TYPE);
            SearchVisibillity = objBundle.getInt(AppConstants.IS_SEARCH_ON, View.GONE);
        }

                token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        objApi = new LG().networkCall(getActivity(), false);
        mListFinal2 = new ArrayList<>();

        if (SearchVisibillity == View.GONE) {
            llSearchGateway.setVisibility(View.GONE);
            resetSearchFields();
        } else {
            llSearchGateway.setVisibility(View.VISIBLE);
            edtGatwayName.setText(spf.getString(AppConstants.GATEWAY_ID_VALUE_NAME, ""));
            edtGatwayID.setText(spf.getString(AppConstants.GATEWAY_ID_NAV, ""));
        }

        rvGatway.setHasFixedSize(true);
        rvGatway.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvGatway.setLayoutManager(linearLayoutManager);
        adapter = new GatwayDetailsAdapter(getActivity(), mListFinal2, this, type);
        rvGatway.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //dummyData();
                if (mListFinal2.size() < totalcount) {
                    Log.i(AppConstants.TAG, "Pg:" + page);
                    getGatwayList(page + 1, "", "");
                }
            }
        };

        rvGatway.addOnScrollListener(scrollListener);
        rvGatway.setAdapter(adapter);

        btnSearch.setOnClickListener(this);
        llBackProfile.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        edtGatwayID.setOnClickListener(this);
        edtGatwayName.setOnClickListener(this);

        objSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        objSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
                resetSearchFields();
                getGatwayList(1, "", "");
            }
        });

        getGatwayList(1, edtGatwayName.getText().toString(), edtGatwayID.getText().toString());

        ((MainActivity) getActivity()).selectDashboard(false);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(true);
    }

    void resetSearchFields() {
        edtGatwayID.setText("");
        edtGatwayName.setText("");
        SharedPreferences.Editor editor = spf.edit();
        editor.remove(AppConstants.GATEWAY_ID_NAV);
        editor.remove(AppConstants.GATEWAY_ID_VALUE_NAME);
        editor.apply();
    }

    void getGatwayList(int pg, String gname, String gid) {

        if (pg == 1) {
            dialog_wait.show();
            mListFinal2.clear();
            adapter.notifyDataSetChanged();
            progressBarGatway.setVisibility(View.GONE);
        }else
            progressBarGatway.setVisibility(View.VISIBLE);


        objApi.getGatwayListDetails(token, pg, 10, type, gname, gid,AppConstants.IS_SECURED).enqueue(new Callback<GatewayMaster>() {
            @Override
            public void onResponse(Call<GatewayMaster> call, Response<GatewayMaster> response) {
                try {
                    progressBarGatway.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        GatewayMaster count = response.body();
                        com.cl.lg.pojo.GatewayDetails.Data objData = count.getData();
                        mListFinal2.addAll(objData.getList());
                        adapter.notifyDataSetChanged();

                        totalcount = objData.getTotalRecords();
                    } else {
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    }
                    objUtils.dismissProgressDialog(dialog_wait);


                } catch (Exception e) {
                    progressBarGatway.setVisibility(View.GONE);
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
                    objUtils.dismissProgressDialog(dialog_wait);
                }
            }

            @Override
            public void onFailure(Call<GatewayMaster> call, Throwable t) {
                progressBarGatway.setVisibility(View.GONE);
                objUtils.dismissProgressDialog(dialog_wait);
                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.edtGatwayID:
                if (Utils.isInternetAvailable(getActivity())) {
                    getGatway(token, edtGatwayID);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;

            case R.id.edtGatwayName:
                if (Utils.isInternetAvailable(getActivity())) {
                    getGatway(token, edtGatwayName);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;

            case R.id.btnSearch:
                getGatwayList(1, edtGatwayName.getText().toString(), edtGatwayID.getText().toString());
                break;
            case R.id.btnClear:
                resetSearchFields();
                getGatwayList(1, "", "");
                break;
            case R.id.ivFilter:
                resetSearchFields();
                if (llSearchGateway.getVisibility() == View.VISIBLE)
                    llSearchGateway.setVisibility(View.GONE);
                else
                    llSearchGateway.setVisibility(View.VISIBLE);

                SearchVisibillity=llSearchGateway.getVisibility();
                break;
            case R.id.llBackProfile:
                objUtils.loadFragment(new GatewayStatus(), getActivity());
                break;

        }
    }


    void getGatway(String token, final TextInputEditText editText) {
        dialog_wait.show();
        filterList.clear();
        objApi.getGatwayList(token, type,AppConstants.IS_SECURED).enqueue(new Callback<GetGatway>() {

            @Override
            public void onResponse(Call<GetGatway> call, Response<GetGatway> response) {
                objUtils.dismissProgressDialog(dialog_wait);
                if (response.code() == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("1")) {
                        GetGatway objGetCommand = response.body();
                        db.deleteTableData(DBHelper.GATWAY_TABLE_NAME);
                        filterList.clear();
                        for (int i = 0; i < objGetCommand.getData().size(); i++) {
                            CommonDialogResponse objCommonDialogResponse = new CommonDialogResponse();
                            //objCommonDialogResponse.setId(Integer.valueOf(objGetCommand.getData().get(i).getId()));
                            objCommonDialogResponse.setValue(String.valueOf(objGetCommand.getData().get(i).getGatewayName()));
                            objCommonDialogResponse.setId(objGetCommand.getData().get(i).getId());
                            objCommonDialogResponse.setTempId(objGetCommand.getData().get(i).getId().toString());
                            filterList.add(objCommonDialogResponse);
                            db.insertGatway(objGetCommand.getData().get(i).getId(), objGetCommand.getData().get(i).getGatewayName());
                        }
                        dialog_search_list(editText, filterList);
                    } else {
                        Utils.dialogForMessage(getActivity(), objUtils.getStringResourceByName(getActivity(),response.body().getMessage()));
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<GetGatway> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }

    ListDialogSentCommandAdapter objAdapter;
    ArrayList<CommonDialogResponse> mListFilter;
    ArrayList<CommonDialogResponse> mListAll;

    void dialog_search_list(final TextInputEditText editText, ArrayList<CommonDialogResponse> mList) {
        //filterList.clear();
        //filterList = mlistDialog;

        boolean flag = false;
        if (editText == edtGatwayID) {
            flag = true;
        }
        mListFilter = mList;
        mListAll = mList;

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView btGetDirection = dialog.findViewById(R.id.btGetDirection);
        btGetDirection.setVisibility(View.GONE);

        TextView tvNoRecord = dialog.findViewById(R.id.tvNoRecord);
        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final boolean finalFlag = flag;
        final ListDialogSentCommandAdapter objAdapter = new ListDialogSentCommandAdapter(getActivity(), mListFilter, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String value;
                if (finalFlag)
                    value = response.getId().toString();
                else
                    value = response.getValue().toString();

                editText.setText(value);

                SharedPreferences.Editor editor = spf.edit();

                if (editText == edtGatwayID) {
                    //cmd_id = String.valueOf(response.getId());
                    editor.putString(AppConstants.SENT_C_CMD_NAME_VALUE, value);
                    editor.putString(AppConstants.GATEWAY_ID_NAV,value);
                    //editor.putString(AppConstants.SENT_C_CMD_NAME_ID, cmd_id);
                }
                if (editText == edtGatwayName) {
                    //status_id = String.valueOf(response.getId());
                    editor.putString(AppConstants.SENT_C_STATUS_VALUE, value);
                    editor.putString(AppConstants.GATEWAY_ID_VALUE_NAME,value);
                    //editor.putString(AppConstants.SENT_C_STATUS_ID, status_id);
                }
                editor.apply();
                /*if (flag) {
                    cmd_id = String.valueOf(response.getId());
                }
                if(isStatus){
                    status_id=String.valueOf(response.getId());
                }*/
                dialog.dismiss();
            }
        }, flag);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(objAdapter);

        if (mListFilter.size() == 0) {
            tvNoRecord.setVisibility(View.VISIBLE);
            objRecyclerView.setVisibility(View.GONE);
        } else {
            tvNoRecord.setVisibility(View.GONE);
            objRecyclerView.setVisibility(View.VISIBLE);
        }

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        objSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        //ArrayList<CommonDialogResponse> temp = new ArrayList<>();
                        //temp.addAll(mListAll);
                        ArrayList<CommonDialogResponse> temp = new ArrayList<>();
                        temp.addAll(db.getAllGatway());

                        ArrayList<CommonDialogResponse> temp2 = new ArrayList<>();

                        if (editText == edtGatwayID) {
                            temp2=objAdapter.filterId(s, temp);

                        } else {
                            temp2=objAdapter.filter(s, temp);
                        }

                        if (temp2.size() == 0) {
                            tvNoRecord.setVisibility(View.VISIBLE);
                            objRecyclerView.setVisibility(View.GONE);
                        } else {
                            tvNoRecord.setVisibility(View.GONE);
                            objRecyclerView.setVisibility(View.VISIBLE);
                        }
                        //objAdapter.filter(s, temp);
                        /*if (TextUtils.isEmpty(s)) {
                            //mListFilter.clear();
                            //mFiltered=mListAll;
                            objAdapter.notifyDataSetChanged();
                        } else {
                            //Log.i(AppConstants.TAG+"###", mList.size() + "");
                            //filter(s, temp);

                            //objAdapter.filter(s);
                            //objAdapter.getFilter();
                        }*/
                        return false;
                    }
                });

        /*if (editText == edtCommanddname) {
            //getCommands(token, objAdapter, objSearchView, editText);
        } else if (editText == edtStatus) {
            getStatus(token, objAdapter);
        } else if (editText == edtGatwayName) {
            getGatway(token, objAdapter);
        } else*/

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (displayRectangle.height() * 0.9f));
        dialog.show();
    }

    @Override
    public void onClickStatusUI(int position, List objListDetail) {
        navigateDesc(objListDetail);
    }

    @Override
    public void onClickSLC(String slcId) {
        navigate(slcId);
    }

    private void navigateDesc(List objListDetail) {

        GatewayDescription inventory = new GatewayDescription();
        Bundle objBundle = new Bundle();

        objBundle.putString(AppConstants.APN, objListDetail.getDetails().getApnNo());
        objBundle.putString(AppConstants.MOBILE_NUMBER, objListDetail.getDetails().getMobileNumber());
        objBundle.putString(AppConstants.IP, objListDetail.getDetails().getIpAddress());
        objBundle.putString(AppConstants.PORT, objListDetail.getDetails().getPortAddress());
        objBundle.putString(AppConstants.ExtendedPan, objListDetail.getDetails().getExtendedPAN());
        objBundle.putString(AppConstants.SHORT_PAN_ID, objListDetail.getDetails().getShortPAN());
        objBundle.putString(AppConstants.CHANNEL, objListDetail.getDetails().getScanChannel());
        objBundle.putString(AppConstants.LATTITUDE, objListDetail.getLatitude());
        objBundle.putString(AppConstants.LONGITUDE, objListDetail.getLongitude());

        objBundle.putString(AppConstants.GATWAY_TYPE, type);
        objBundle.putInt(AppConstants.IS_SEARCH_ON,SearchVisibillity);
        objBundle.putBoolean(AppConstants.ISFROMMAP, false);
        inventory.setArguments(objBundle);

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment
                //fragmentTransaction.replace(R.id.frm1, inventory);
                //fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
               //fragmentTransaction.commitAllowingStateLoss();


                fragmentTransaction.add(R.id.frm1, inventory);
                fragmentTransaction.hide(GatewayInventory.this);
                fragmentTransaction.addToBackStack(GatewayInventory.class.getName());
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
        }

    }

    void navigate(String type1) {

        AssignedSLC inventory = new AssignedSLC();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.SLC_GROUP_ID, type1);
        objBundle.putString(AppConstants.GATWAY_TYPE, type);
        objBundle.putInt(AppConstants.IS_SEARCH_ON,SearchVisibillity);
        inventory.setArguments(objBundle);

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment
                //fragmentTransaction.replace(R.id.frm1, inventory);
                //fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
                //fragmentTransaction.commitAllowingStateLoss();

                fragmentTransaction.add(R.id.frm1, inventory);
                fragmentTransaction.hide(GatewayInventory.this);
                fragmentTransaction.addToBackStack(GatewayInventory.class.getName());
                fragmentTransaction.commit();
            }
        } catch (Exception e) {
        }
    }

}