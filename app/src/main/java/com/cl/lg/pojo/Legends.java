package com.cl.lg.pojo;

import android.graphics.drawable.Drawable;

public class Legends {

    String name,per;
    int colorCode;
    float perF;
    Drawable drawable;
    String displayName;
    String GraphName;

    public String getGraphName() {
        return GraphName;
    }

    public void setGraphName(String graphName) {
        GraphName = graphName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }

    public float getPerF() {
        return perF;
    }

    public void setPerF(float perF) {
        this.perF = perF;
    }

    public int getColorCode() {
        return colorCode;
    }

    public void setColorCode(int colorCode) {
        this.colorCode = colorCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPer() {
        return per;
    }

    public void setPer(String per) {
        this.per = per;
    }

}
