package com.cl.lg.activities;

import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

class JWTUtils {


    public static String decoded(String JWTEncoded) throws Exception {
        String strBody = "";
        String strExp = "";
        String strStart = "";
        String strUserName = "";
        try {
            String[] split = JWTEncoded.split("\\.");
            Log.d("JWT_DECODED", "Header: " + getJson(split[0]));
            Log.d("JWT_DECODED", "Body: " + getJson(split[1]));

            //strBody=getClientId(getJson(split[1]));
            //strExp=getExpireSession(getJson(split[1]));
            //strStart=getStartSession(getJson(split[1]));

            strUserName = getUser(getJson(split[1]));

            if (strUserName.isEmpty()) {
                strUserName = getUserUPN(getJson(split[1]));
            }
        } catch (UnsupportedEncodingException e) {
            //Error
        }
        //return strBody+":"+strStart+":"+strExp+":"+strUserName;
        return strUserName+":";
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }

       /* private static String getClientId(String body){

            try {
                JSONObject jsonObject=new JSONObject(body);
                return jsonObject.get("custom:clientId").toString();

            } catch (JSONException e) {
                e.printStackTrace();
                return "";
            }

        }*/

      /*  private static String getExpireSession(String body){

            try {
                JSONObject jsonObject=new JSONObject(body);
                return jsonObject.get("exp").toString();

            } catch (JSONException e) {
                e.printStackTrace();
                return "";
            }

        }*/

    /* private static String getStartSession(String body){

         try {
             JSONObject jsonObject=new JSONObject(body);
             return jsonObject.get("iat").toString();

         } catch (JSONException e) {
             e.printStackTrace();
             return "";
         }

     }*/
    private static String getUser(String body) {

        try {
            JSONObject jsonObject = new JSONObject(body);
            return jsonObject.get("unique_name").toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }

    }

    private static String getUserUPN(String body) {

        try {
            JSONObject jsonObject = new JSONObject(body);
            return jsonObject.get("upn").toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }

    }


}
