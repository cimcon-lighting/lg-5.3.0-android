
package com.cl.lg.pojo.SentCommand;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendCommandBodyInner {

    @SerializedName("SLCID")
    @Expose
    private String sLCID;
    @SerializedName("Gateway")
    @Expose
    private String gateway;
    @SerializedName("CommandName")
    @Expose
    private String commandName;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("SentBy")
    @Expose
    private String sentBy;
    @SerializedName("TrackId")
    @Expose
    private String trackId;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("ToDate")
    @Expose
    private String toDate;

    public String getSLCID() {
        return sLCID;
    }

    public void setSLCID(String sLCID) {
        this.sLCID = sLCID;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

}
