package com.cl.lg.pojo;

public class CommonResponseStatusDetailsDialog{

    String id;
    String Value;
    String from;
    boolean isForLSComm;

    public boolean isForLSComm() {
        return isForLSComm;
    }

    public void setForLSComm(boolean forLSComm) {
        isForLSComm = forLSComm;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
