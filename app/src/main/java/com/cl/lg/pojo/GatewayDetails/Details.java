
package com.cl.lg.pojo.GatewayDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Details {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("gatewayName")
    @Expose
    private String gatewayName;
    @SerializedName("gatewayDescription")
    @Expose
    private String gatewayDescription;
    @SerializedName("communication_Type")
    @Expose
    private String communicationType;
    @SerializedName("daylightStatus")
    @Expose
    private String daylightStatus;
    @SerializedName("logRate")
    @Expose
    private String logRate;
    @SerializedName("apnNo")
    @Expose
    private String apnNo;
    @SerializedName("ipAddress")
    @Expose
    private String ipAddress;
    @SerializedName("portAddress")
    @Expose
    private String portAddress;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("extendedPAN")
    @Expose
    private String extendedPAN;
    @SerializedName("shortPAN")
    @Expose
    private String shortPAN;
    @SerializedName("scanChannel")
    @Expose
    private String scanChannel;
    @SerializedName("gatewayVersion")
    @Expose
    private String gatewayVersion;
    @SerializedName("tempId")
    @Expose
    private Integer tempId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(String gatewayName) {
        this.gatewayName = gatewayName;
    }

    public String getGatewayDescription() {
        return gatewayDescription;
    }

    public void setGatewayDescription(String gatewayDescription) {
        this.gatewayDescription = gatewayDescription;
    }

    public String getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(String communicationType) {
        this.communicationType = communicationType;
    }

    public String getDaylightStatus() {
        return daylightStatus;
    }

    public void setDaylightStatus(String daylightStatus) {
        this.daylightStatus = daylightStatus;
    }

    public String getLogRate() {
        return logRate;
    }

    public void setLogRate(String logRate) {
        this.logRate = logRate;
    }

    public String getApnNo() {
        return apnNo;
    }

    public void setApnNo(String apnNo) {
        this.apnNo = apnNo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPortAddress() {
        return portAddress;
    }

    public void setPortAddress(String portAddress) {
        this.portAddress = portAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getExtendedPAN() {
        return extendedPAN;
    }

    public void setExtendedPAN(String extendedPAN) {
        this.extendedPAN = extendedPAN;
    }

    public String getShortPAN() {
        return shortPAN;
    }

    public void setShortPAN(String shortPAN) {
        this.shortPAN = shortPAN;
    }

    public String getScanChannel() {
        return scanChannel;
    }

    public void setScanChannel(String scanChannel) {
        this.scanChannel = scanChannel;
    }

    public String getGatewayVersion() {
        return gatewayVersion;
    }

    public void setGatewayVersion(String gatewayVersion) {
        this.gatewayVersion = gatewayVersion;
    }

    public Integer getTempId() {
        return tempId;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

}
