package com.cl.lg.pojo.SLCStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("list")
    @Expose
    private java.util.List<com.cl.lg.pojo.SLCStatus.List> list = null;
    @SerializedName("totalRecords")
    @Expose
    private String totalRecords;

    @SerializedName("filterCounts")
    @Expose
    private FilterCounts filterCounts;

    public FilterCounts getFilterCounts() {
        return filterCounts;
    }

    public void setFilterCounts(FilterCounts filterCounts) {
        this.filterCounts = filterCounts;
    }

    public java.util.List<com.cl.lg.pojo.SLCStatus.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.cl.lg.pojo.SLCStatus.List> list) {
        this.list = list;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

}