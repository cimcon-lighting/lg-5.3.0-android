package com.cl.lg.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cl.lg.R;
import com.cl.lg.pojo.CommonResponseStatusDetailsDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDialogStatusAdapter extends RecyclerView.Adapter<ListDialogStatusAdapter.MyViewHolderList> {

    Context mContext;
    List<CommonResponseStatusDetailsDialog> mList;
    boolean isFromHistory;
    boolean isFromDayBurnner;
    boolean isHistoryBottonVisible;
    //private MyCallbackForControlDialog objMyCallbackForControl;

    /*public interface MyCallbackForControlDialog {
        void onClickForControlStatus(int position, String slcNo);
    }*/

    public ListDialogStatusAdapter(Context mContext, List<CommonResponseStatusDetailsDialog> mList) {
        this.mList = mList;
        //this.objMyCallbackForControl = objMyCallbackForControl;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolderList onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.raw_status_dialog_list, viewGroup, false);
        MyViewHolderList viewHolder = new MyViewHolderList(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderList myViewHolder, final int i) {

        if (mList.get(i).getFrom().equalsIgnoreCase("s")) {
            myViewHolder.tvSLC.setVisibility(View.GONE);
            myViewHolder.ivSLCDots.setVisibility(View.VISIBLE);

            //lamp status & comm  0-red 1- green
            //Logic for static and dynamic
            if (mList.get(i).isForLSComm()) {
                if (mList.get(i).getValue().equalsIgnoreCase("0"))
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));
                else
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));
            } else {
                if (mList.get(i).getValue().equalsIgnoreCase("0"))
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));
                else if (mList.get(i).getValue().equalsIgnoreCase("") || mList.get(i).getValue().isEmpty()) {
                    myViewHolder.ivSLCDots.setVisibility(View.GONE);
                } else
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));
            }

        } else if (mList.get(i).getFrom().equalsIgnoreCase("p")) {
            myViewHolder.tvSLC.setVisibility(View.VISIBLE);
            myViewHolder.ivSLCDots.setVisibility(View.GONE);
        }

        try {
            myViewHolder.tvSLC.setText(mList.get(i).getValue().toString());
        } catch (Exception e) {
            myViewHolder.tvSLC.setText("");
        }

        try {
            myViewHolder.tvSLCLbl.setText(mList.get(i).getId().toString());
        } catch (Exception e) {
            myViewHolder.tvSLCLbl.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class MyViewHolderList extends RecyclerView.ViewHolder {

        @BindView(R.id.llTitles)
        LinearLayout llTitles;

        @BindView(R.id.tvSLC)
        TextView tvSLC;

        @BindView(R.id.tvSLCLbl)
        TextView tvSLCLbl;

        @BindView(R.id.ivSLCDots)
        ImageView ivSLCDots;

        public MyViewHolderList(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
/*if(mList.get(i).getId().equalsIgnoreCase("Lamp Status") || mList.get(i).getId().equalsIgnoreCase("Communication")){
                if (mList.get(i).getValue().equalsIgnoreCase("0"))
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));
                else
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));
            }else {

                if (mList.get(i).getValue().equalsIgnoreCase("0"))
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_green));
                else
                    myViewHolder.ivSLCDots.setImageDrawable(mContext.getResources().getDrawable(R.drawable.custom_round_red));
            }*/
