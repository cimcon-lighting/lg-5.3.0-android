package com.cl.lg.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;

import com.cl.lg.activities.MainActivity;
import com.google.android.material.textfield.TextInputEditText;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cl.lg.LG;
import com.cl.lg.R;
import com.cl.lg.adapters.AssignedSLCAdapter;
import com.cl.lg.adapters.ListDialogSentCommandAdapter;
import com.cl.lg.networking.API;
import com.cl.lg.pojo.AssignedSLC.AssignedMaster;
import com.cl.lg.pojo.AssignedSLC.Data;
import com.cl.lg.pojo.AssignedSLC.ListSLC;
import com.cl.lg.pojo.CommonDialogResponse;
import com.cl.lg.pojo.SLCName.SLCName;
import com.cl.lg.utils.AppConstants;
import com.cl.lg.utils.EndlessRecyclerViewScrollListener;
import com.cl.lg.utils.Utils;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssignedSLC extends Fragment implements View.OnClickListener, AssignedSLCAdapter.MyCallbackForControl {

    @BindView(R.id.llBackProfile)
    LinearLayout llBackProfile;

    @BindView(R.id.ivFilter)
    ImageView ivFilter;

    @BindView(R.id.llSearchGateway)
    LinearLayout llSearchGateway;

    @BindView(R.id.edtSLCID)
    TextInputEditText edtSLCID;

    @BindView(R.id.edtSLCName)
    TextInputEditText edtSLCName;

    @BindView(R.id.edtMacAddress)
    TextInputEditText edtMacAddress;

    @BindView(R.id.btnSearch)
    Button btnSearch;

    @BindView(R.id.btnClear)
    Button btnClear;

    @BindView(R.id.rvAssignedSlc)
    RecyclerView rvAssignedSlc;

    @BindView(R.id.swpRefresshPole)
    SwipeRefreshLayout objSwipeRefreshLayout;

    @BindView(R.id.progressBarAssigned)
    ProgressBar progressBarAssigned;

    Utils objUtils;

    View view;

    API objApi;
    String token;

    ProgressDialog dialog_wait;
    SharedPreferences spf;
    String rtuGroupID = "";
    String Gtype = "";
    Bundle objBundle;

    AssignedSLCAdapter adapter;
    EndlessRecyclerViewScrollListener scrollListener;
    int totalcount;

    java.util.List<ListSLC> mListFinal2;

    final ArrayList<CommonDialogResponse> mListSearch = new ArrayList<>();
    private FirebaseAnalytics mFirebaseAnalytics;

    String lamptypid;

    int searchvisibility;
    boolean isSingNodeType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_assigned_slc, null);
        init();
        return view;
    }

    void init() {
        ButterKnife.bind(this, view);
        objUtils = new Utils();


        dialog_wait = new ProgressDialog(getActivity());
        dialog_wait.setMessage(getResources().getString(R.string.please_wait));
        dialog_wait.setCancelable(false);

        if (getArguments() != null) {
            objBundle = getArguments();
            rtuGroupID = objBundle.getString(AppConstants.SLC_GROUP_ID);
            Gtype = objBundle.getString(AppConstants.GATWAY_TYPE);
            searchvisibility = objBundle.getInt(AppConstants.IS_SEARCH_ON, View.GONE);
        }

        spf = getActivity().getSharedPreferences(AppConstants.SPF, Context.MODE_PRIVATE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setCurrentScreen(getActivity(), "assignedSLC", null /* class override */);

        isSingNodeType = spf.getBoolean(AppConstants.isSingleNodeType, false);
        token = spf.getString(AppConstants.ACCESS_TOKEN, "");
        objApi = new LG().networkCall(getActivity(), false);
        mListFinal2 = new ArrayList<>();
        lamptypid = spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID, "");
        rvAssignedSlc.setHasFixedSize(true);
        rvAssignedSlc.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvAssignedSlc.setLayoutManager(linearLayoutManager);
        adapter = new AssignedSLCAdapter(getActivity(), mListFinal2, this, rtuGroupID);
        rvAssignedSlc.addItemDecoration(new DividerItemDecoration(getActivity(), 0));

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //dummyData();
                if (mListFinal2.size() < totalcount) {
                    Log.i(AppConstants.TAG, "Pg:" + page);
                    getAssingedSLC(token, page + 1);
                }
            }
        };

        rvAssignedSlc.addOnScrollListener(scrollListener);
        rvAssignedSlc.setAdapter(adapter);

        btnSearch.setOnClickListener(this);
        llBackProfile.setOnClickListener(this);
        ivFilter.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        edtSLCID.setOnClickListener(this);

        objSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Do something after 3000ms
                        objSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 2500);
                resetSearchFields();
                getAssingedSLC(token, 1);
            }
        });

        getAssingedSLC(token, 1);


        ((MainActivity) getActivity()).selectDashboard(false);
        ((MainActivity) getActivity()).selectStatus(false);
        ((MainActivity) getActivity()).selectMap(false);
        ((MainActivity) getActivity()).selectMenu(true);
    }

    void resetSearchFields() {
        edtSLCID.setText("");
        edtSLCName.setText("");
        edtMacAddress.setText("");
    }

    void getAssingedSLC(String token, int pg) {

        if (pg == 1) {
            dialog_wait.show();
            mListFinal2.clear();
            adapter.notifyDataSetChanged();
            progressBarAssigned.setVisibility(View.GONE);
        } else
            progressBarAssigned.setVisibility(View.VISIBLE);

        HashMap<String, String> map = new HashMap<>();
        map.put("SLCNO", edtSLCID.getText().toString());
        map.put("SLCName", edtSLCName.getText().toString());
        //map.put("MACAddress", edtMacAddress.getText().toString());
        map.put("UID", edtMacAddress.getText().toString());
        map.put("GatewayID", rtuGroupID);


        objApi.getAssignedSLC(token, pg, 10, map, AppConstants.IS_SECURED).enqueue(new Callback<AssignedMaster>() {
            @Override
            public void onResponse(Call<AssignedMaster> call, Response<AssignedMaster> response) {
                try {
                    progressBarAssigned.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        AssignedMaster count = response.body();
                        Data objData = count.getData();

                        mListFinal2.addAll(objData.getListSLC());
                        adapter.notifyDataSetChanged();
                        totalcount = objData.getTotalRecords();

                    } else {
                        objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
                    }
                    objUtils.dismissProgressDialog(dialog_wait);

                } catch (Exception e) {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
                    objUtils.dismissProgressDialog(dialog_wait);
                    progressBarAssigned.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AssignedMaster> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
                progressBarAssigned.setVisibility(View.GONE);
                Utils.dialogForMessage(getActivity(), getResources().getString(R.string.http_error_msg));
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSearch:
                getAssingedSLC(token, 1);
                break;
            case R.id.btnClear:
                resetSearchFields();
                getAssingedSLC(token, 1);
                break;
            case R.id.ivFilter:
                String event_name = spf.getString(AppConstants.GLOBAL_EVENT_NAME, "") + "searchAssignedSlc";
                Log.d(AppConstants.TAG, "Event:" + event_name);
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, event_name);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                resetSearchFields();
                if (llSearchGateway.getVisibility() == View.VISIBLE)
                    llSearchGateway.setVisibility(View.GONE);
                else
                    llSearchGateway.setVisibility(View.VISIBLE);
                break;
            case R.id.llBackProfile:
                //navigate(Gtype);
                getActivity().onBackPressed();
                break;
            case R.id.edtSLCID:
                if (Utils.isInternetAvailable(getActivity())) {
                    //Utils.hideKeyboard(getActivity());
                    dialog_search_list(edtSLCID);
                } else {
                    Utils.dialogForMessage(getActivity(), getResources().getString(R.string.no_internet_connection));
                }
                break;
        }
    }

    void navigate(String type) {

        GatewayInventory inventory = new GatewayInventory();
        Bundle objBundle = new Bundle();
        objBundle.putString(AppConstants.GATWAY_TYPE, type);
        objBundle.putInt(AppConstants.IS_SEARCH_ON, searchvisibility);
        inventory.setArguments(objBundle);

        try {
            if (getActivity() != null) {
                // create a FragmentManager
                FragmentManager fm = getActivity().getFragmentManager();
                // create a FragmentTransaction to begin the transaction and replace the Fragment
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                // replace the FrameLayout with new Fragment
                fragmentTransaction.replace(R.id.frm1, inventory);
                fragmentTransaction.addToBackStack(null);
                //fragmentTransaction.commit(); // save the changes
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (Exception e) {
        }
    }

    String slc_id;

    void dialog_search_list(final TextInputEditText editText) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_serach_list);
        dialog.setCancelable(false);

        TextView dialogButtondir = dialog.findViewById(R.id.btGetDirection);
        dialogButtondir.setVisibility(View.GONE);

        TextView dialogButton = dialog.findViewById(R.id.btCancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        RecyclerView objRecyclerView = dialog.findViewById(R.id.rvDialog);

        objRecyclerView.setHasFixedSize(true);

        objRecyclerView.setItemViewCacheSize(20);
        objRecyclerView.setDrawingCacheEnabled(true);
        objRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        objRecyclerView.setLayoutManager(linearLayoutManager);
        final ListDialogSentCommandAdapter adapterDialog = new ListDialogSentCommandAdapter(getActivity(), mListSearch,false, new ListDialogSentCommandAdapter.MyCallbackForControlDialog() {
            @Override
            public void onClickForControl(int position, CommonDialogResponse response) {
                String displayText = response.getValue().toString();
                editText.setText(displayText);

                slc_id = response.getId_str();
                spf.edit().putString(AppConstants.SLC_ID_NAV, slc_id).apply();
                spf.edit().putString(AppConstants.SLC_ID_VALUE_NAVE, displayText).apply();
                dialog.dismiss();
            }
        });
        objRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 0));
        objRecyclerView.setAdapter(adapterDialog);

        getSLCName("", adapterDialog, dialog);

        androidx.appcompat.widget.SearchView objSearchView = dialog.findViewById(R.id.svSLCs);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        objSearchView.setSearchableInfo(searchManager
                .getSearchableInfo(getActivity().getComponentName()));

        objSearchView.setQueryHint(getResources().getString(R.string.search));
        objSearchView.setMaxWidth(Integer.MAX_VALUE);

        objSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (editText == edtSLCID) {
                    if (s.toString().trim().equalsIgnoreCase("")) {
                        getSLCName("", adapterDialog, dialog);
                    } else {
                        getSLCName(s.toString(), adapterDialog, dialog);
                    }
                }
                return false;
            }
        });

        // dialogButton.setOnTouchListener(Util.colorFilter());

        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();

        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        dialog.getWindow().setLayout((int) (displayRectangle.width() * 0.9f), (int) (1300));
        dialog.show();
    }

    void getSLCName(String slc, final ListDialogSentCommandAdapter adapter, Dialog dialog) {
        //dialog_wait.show();
        mListSearch.clear();
        adapter.notifyDataSetChanged();

        //APPS 381 issue, here we are passing lamptyid 0
        //lamptypid=spf.getString(AppConstants.SELECTED_LAMP_TYPE_ID,"");
        String nodeType;
        if (isSingNodeType)
            nodeType = "";
        else
            nodeType = spf.getString(AppConstants.SELECTED_NODE_TYPE_ID, "");

        objApi.getSLCName(token, slc, rtuGroupID, "0", AppConstants.IS_SECURED, "").enqueue(new Callback<SLCName>() {
            @Override
            public void onResponse(Call<SLCName> call, Response<SLCName> response) {
                objUtils.dismissProgressDialog(dialog_wait);

                if (response.code() == 200) {
                    SLCName slcName = response.body();

                    if (slcName.getStatus().equalsIgnoreCase("1")) {

                        if (slcName.getData().size() == 0) {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                        } else {
                            dialog.findViewById(R.id.rvDialog).setVisibility(View.VISIBLE);
                            dialog.findViewById(R.id.tvNoRecord).setVisibility(View.GONE);
                        }
                        mListSearch.clear();
                        for (int i = 0; i < slcName.getData().size(); i++) {
                            CommonDialogResponse response1 = new CommonDialogResponse();
                            //response1.setValue(slcName.getData().get(i).getText());
                            response1.setValue(slcName.getData().get(i).getValue());
                            response1.setId_str(slcName.getData().get(i).getValue());
                            response1.setType(slcName.getData().get(i).getValue());
                            response1.setType("SLC_NAME");
                            mListSearch.add(response1);
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        dialog.findViewById(R.id.rvDialog).setVisibility(View.GONE);
                        dialog.findViewById(R.id.tvNoRecord).setVisibility(View.VISIBLE);
                    }
                } else
                    objUtils.responseHandle(getActivity(), response.code(), response.errorBody());
            }

            @Override
            public void onFailure(Call<SLCName> call, Throwable t) {
                objUtils.dismissProgressDialog(dialog_wait);
            }
        });
    }
}